<?php

namespace Crm;

use Crm\App\Models\Lead;
use Crm\App\Policies\LeadResourcePolicy;

class CrmAuthServiceProvider
{
    public static array $policies = [
        Lead::class => LeadResourcePolicy::class,
    ];
}
