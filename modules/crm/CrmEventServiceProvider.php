<?php

namespace Crm;

use Crm\App\Listeners\SetCrmConnectionOnUserLogin;
use Illuminate\Auth\Events\Login;

class CrmEventServiceProvider
{
    public static array $listen = [
        Login::class => [
            SetCrmConnectionOnUserLogin::class,
        ]
    ];
}
