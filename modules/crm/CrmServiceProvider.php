<?php

namespace Crm;

use Crm\App\Console\Commands\CrmMakeConsole;
use Crm\App\Console\Commands\CrmMakeDatabase;
use Crm\App\Console\Commands\CrmMakeFactory;
use Crm\App\Console\Commands\CrmMakeMigration;
use Crm\App\Console\Commands\CrmMakeModel;
use Crm\App\Console\Commands\CrmMakePolicy;
use Crm\App\Console\Commands\CrmMakeSeeder;
use Crm\App\Console\Commands\CrmMakeService;
use Crm\App\Console\Commands\CrmMigrateDatabases;
use Crm\App\Console\Commands\CrmReseedDatabase;
use Crm\App\Console\Commands\CrmRollbackDatabases;
use Crm\App\Console\Commands\CrmSeedDatabase;
use Crm\App\Services\Multitenancy\Contracts\DbConnectionContract;
use Crm\App\Services\Multitenancy\Contracts\DbCreationContract;
use Crm\App\Services\Multitenancy\Contracts\DbMigrationContract;
use Crm\App\Services\Multitenancy\Contracts\DbRollbackContract;
use Crm\App\Services\Multitenancy\Contracts\DbSeederContract;
use Crm\App\Services\Multitenancy\DbConnectionService;
use Crm\App\Services\Multitenancy\DbCreationService;
use Crm\App\Services\Multitenancy\DbMigrationService;
use Crm\App\Services\Multitenancy\DbRollbackService;
use Crm\App\Services\Multitenancy\DbSeederService;
use Crm\App\Services\Rights\Contracts\ResourceRightsContract;
use Crm\App\Services\Rights\ResourceRightsService;
use Illuminate\Support\ServiceProvider;

class CrmServiceProvider extends ServiceProvider
{
    #region PROPERTIES
    public array $bindings = [
        DbConnectionContract::class => DbConnectionService::class,
        DbCreationContract::class => DbCreationService::class,
        DbMigrationContract::class => DbMigrationService::class,
        DbRollbackContract::class => DbRollbackService::class,
        DbSeederContract::class => DbSeederService::class,
        ResourceRightsContract::class => ResourceRightsService::class,
    ];
    #endregion

    #region MAIN METHODS
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/crm.php', 'crm');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/crm.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadFactoriesFrom(__DIR__.'/database/factories');
        $this->registerConsoleCommands();
    }
    #endregion

    #region SERVICE METHODS
    private function registerConsoleCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CrmMakeModel::class,
                CrmMakeService::class,
                CrmMakeConsole::class,
                CrmMakeDatabase::class,
                CrmMakeMigration::class,
                CrmMigrateDatabases::class,
                CrmRollbackDatabases::class,
                CrmSeedDatabase::class,
                CrmReseedDatabase::class,
                CrmMakeFactory::class,
                CrmMakeSeeder::class,
                CrmMakePolicy::class,
            ]);
        }
    }
    #endregion
}
