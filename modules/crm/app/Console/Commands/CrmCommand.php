<?php

namespace Crm\app\Console\Commands;

use Illuminate\Console\Command;

abstract class CrmCommand extends Command
{
    #region CONSTANTS
    public const ALERT = 'alert';
    public const ERROR = 'error';
    public const INFO = 'info';
    #endregion

    #region SERVICE METHODS
    /**
     * @param array $executionReport
     */
    protected function reportExecution(array $executionReport): void
    {
        foreach ($executionReport as $type => $messages) {
            if (in_array($type, [self::ALERT, self::ERROR, self::INFO])) {
                foreach ($messages as $message) {
                    $this->$type($message);
                }
            }
        }
    }
    #endregion
}
