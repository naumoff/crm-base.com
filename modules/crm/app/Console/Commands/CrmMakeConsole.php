<?php

namespace Crm\App\Console\Commands;

use Crm\Stubs\Console\Services\CrmConsoleCreator;
use Illuminate\Console\Command;

class CrmMakeConsole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.make:command {commandName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creation of CRM console command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $commandName = $this->argument('commandName');

        /** @var CrmConsoleCreator $creator */
        $creator = app()->make(CrmConsoleCreator::class);
        $creator->create($commandName, base_path('modules/crm/app/Console/Commands'));
        $this->info('CRM console command created!');
    }
}
