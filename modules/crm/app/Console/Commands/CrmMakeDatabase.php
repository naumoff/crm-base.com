<?php

namespace Crm\App\Console\Commands;

use Crm\App\Services\Multitenancy\Contracts\DbConnectionContract;
use Crm\App\Services\Multitenancy\Contracts\DbCreationContract;
use Illuminate\Console\Command;

class CrmMakeDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.make:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command creates new database for crm data';

    /** @var DbConnectionContract $dbConnectionService */
    private DbConnectionContract $dbConnectionService;

    /** @var DbCreationContract $dbCreationService */
    private DbCreationContract $dbCreationService;

    /**
     * CrmMakeDatabase constructor.
     * @param DbConnectionContract $dbConnectionService
     * @param DbCreationContract $dbCreationService
     */
    public function __construct(DbConnectionContract $dbConnectionService, DbCreationContract $dbCreationService)
    {
        parent::__construct();
        $this->dbConnectionService = $dbConnectionService;
        $this->dbCreationService = $dbCreationService;
    }

    /**
     * @throws \ReflectionException
     */
    public function handle(): void
    {
        $dbName = $this->dbCreationService->createNewDb($this->dbConnectionService->getFreeConnection());
        if ($dbName) {
            $this->info($dbName . ' database created successfully!');
        } else {
            $this->alert('Database was not created, sth goes wrong!');
        }
    }
}
