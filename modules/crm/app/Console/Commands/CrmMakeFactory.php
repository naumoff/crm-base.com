<?php

namespace Crm\App\Console\Commands;

use Crm\Stubs\Factories\Services\CrmFactoryCreator;

class CrmMakeFactory extends CrmCommand
{
    /** @var CrmFactoryCreator */
    private CrmFactoryCreator $crmFactoryCreator;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.make:factory {fileName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command creates crm factory';

    /**
     * CrmMakeFactory constructor.
     * @param CrmFactoryCreator $crmFactoryCreator
     */
    public function __construct(CrmFactoryCreator $crmFactoryCreator)
    {
        parent::__construct();
        $this->crmFactoryCreator = $crmFactoryCreator;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $fileName = $this->argument('fileName');
        $this->crmFactoryCreator->create($fileName, base_path('modules/crm/database/factories'));
        $this->info('CRM factory created!');
    }
}
