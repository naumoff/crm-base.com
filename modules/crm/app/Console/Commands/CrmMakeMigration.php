<?php

namespace Crm\App\Console\Commands;

use Crm\Stubs\Migrations\Services\CrmMigrationCreator;
use Illuminate\Console\Command;

class CrmMakeMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.make:migration {name} {--create=} {--update=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command creates crm migration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle(): void
    {
        $migrationName = $this->argument('name');

        if ($this->option('create')) {
            $table = $this->option('create');
            $create = true;
        } elseif ($this->option('update')) {
            $table = $this->option('update');
            $create = false;
        } else {
            $table = null;
            $create = false;
        }

        /** @var CrmMigrationCreator $creator */
        $creator = app()->make(CrmMigrationCreator::class);
        $creator->create($migrationName, base_path('/modules/crm/database/migrations/crm'), $table, $create);
        $this->info('CRM migration created!');
    }
}
