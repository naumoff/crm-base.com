<?php

namespace Crm\App\Console\Commands;

use Crm\Stubs\Models\Services\CrmModelCreator;
use Illuminate\Console\Command;

class CrmMakeModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.make:model {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = $this->argument('model');
        /** @var CrmModelCreator $creator */
        $creator = app()->make(CrmModelCreator::class);
        $creator->create($model, base_path('modules/crm/app/Models'));
        $this->info($model . ' eloquent model successfully created');
    }
}
