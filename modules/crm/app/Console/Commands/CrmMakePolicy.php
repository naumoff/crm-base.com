<?php

namespace Crm\App\Console\Commands;

use Crm\Stubs\Policies\Services\CrmPolicyCreator;

class CrmMakePolicy extends CrmCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.make:policy {name} {--model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command creates CRM policy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws \ReflectionException
     */
    public function handle()
    {
        $policyName = $this->argument('name') . 'Policy';
        $modelName = ($this->option('model'))? ucfirst($this->argument('name')) : null;
        if ($modelName) {
            if (class_exists($modelClass = "Crm\App\Models\\" . $modelName)) {
                $model = new $modelClass;
            } elseif (class_exists($modelClass = "App\\" . $modelName)) {
                $model = new $modelClass;
            } else {
                $this->error("Cannot find eloquent model for class {$modelName}");
                return;
            }
        }
        /** @var CrmPolicyCreator $creator */
        $creator = app()->make(CrmPolicyCreator::class);
        $policyPath = $creator->create($policyName, $model ?? null);
        $this->info("Policy {$policyPath} created successfully!");
    }
}
