<?php

namespace Crm\App\Console\Commands;

use Crm\Stubs\Seeds\Services\CrmSeederCreator;

class CrmMakeSeeder extends CrmCommand
{
    /** @var CrmSeederCreator */
    private CrmSeederCreator $crmSeederCreator;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.make:seeder {className}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command will created CRM seeder';

    /**
     * CrmMakeSeeder constructor.
     * @param CrmSeederCreator $crmSeederCreator
     */
    public function __construct(CrmSeederCreator $crmSeederCreator)
    {
        parent::__construct();
        $this->crmSeederCreator = $crmSeederCreator;
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $className = $this->argument('className');
        $this->crmSeederCreator->create($className, base_path('modules/crm/database/seeds'));
        $this->info('CRM seeder created!');
    }
}
