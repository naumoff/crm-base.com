<?php

namespace Crm\App\Console\Commands;

use Crm\Stubs\Services\Services\CrmServiceCreator;
use Illuminate\Console\Command;

class CrmMakeService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.make:service {service} {--folder=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to make CRM service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $service = $this->argument('service');
        $folder = $this->option('folder');

        /** @var CrmServiceCreator $creator */
        $creator = app()->make(CrmServiceCreator::class);
        $creator->create($folder, $service, base_path('modules/crm/app/Services'));
        $this->info($service . ' service created successfully in folder ' . $folder);
    }
}
