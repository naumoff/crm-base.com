<?php

namespace Crm\App\Console\Commands;

use Crm\App\Services\Multitenancy\Contracts\DbMigrationContract;

class CrmMigrateDatabases extends CrmCommand
{
    #region PROPERTIES
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.migrate {--connect=} {--db=} {--step=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command migrate multi tenant databases';
    #endregion

    #region MAIN METHODS
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function handle()
    {
        $connectId = $this->option('connect');
        $dbId = $this->option('db');
        $step = $this->option('step');
        /** @var DbMigrationContract $migrator */
        $migrator = app()->make(DbMigrationContract::class);
        $migrationReport = $migrator->migrate($connectId, $dbId, $step);
        $this->reportExecution($migrationReport);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
