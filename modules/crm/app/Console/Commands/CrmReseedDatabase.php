<?php

namespace Crm\App\Console\Commands;

class CrmReseedDatabase extends CrmCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.db:reseed {db}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command will refresh and reseed database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('app.env') === 'production') {
            $this->reportExecution([self::ERROR => ['No seeders on production server!']]);
            return;
        }
        $dbId = $this->argument('db');
        $this->call('crm.migrate:rollback', ['--db' => $dbId]);
        $this->call('crm.migrate', ['--db' => $dbId]);
        $this->call('crm.db:seed', ['db' => $dbId]);
    }
}
