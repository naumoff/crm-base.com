<?php

namespace Crm\App\Console\Commands;

use Crm\App\Services\Multitenancy\Contracts\DbRollbackContract;

class CrmRollbackDatabases extends CrmCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.migrate:rollback {--connect=} {--db=} {--step=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command rollback multi tenant databases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connectId = $this->option('connect');
        $dbId = $this->option('db');
        $step = $this->option('step');
        $rollbackResult = app()->make(DbRollbackContract::class)->rollback($connectId, $dbId, $step);
        $this->reportExecution($rollbackResult);
    }
}
