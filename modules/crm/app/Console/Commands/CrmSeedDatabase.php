<?php

namespace Crm\App\Console\Commands;

use Crm\App\Services\Multitenancy\Contracts\DbSeederContract;

class CrmSeedDatabase extends CrmCommand
{
    /** @var DbSeederContract */
    private DbSeederContract $dbSeederService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm.db:seed {db} {--seeder=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command will seed test crm db with fake data';

    /**
     * CrmSeedDatabase constructor.
     * @param DbSeederContract $dbSeederContract
     */
    public function __construct(DbSeederContract $dbSeederContract)
    {
        parent::__construct();
        $this->dbSeederService = $dbSeederContract;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dbId = $this->argument('db');
        $seeder = $this->option('seeder');
        $seederReport = $this->dbSeederService->seed($dbId, $seeder);
        $this->reportExecution($seederReport);
    }
}
