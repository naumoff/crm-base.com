<?php

namespace Crm\App\Listeners;

use Crm\App\Services\Multitenancy\Contracts\DbConnectionContract;

class SetCrmConnectionOnUserLogin
{
    /** @var DbConnectionContract */
    private DbConnectionContract $dbConnectionService;

    /**
     * SetCrmConnectionOnUserLogin constructor.
     * @param DbConnectionContract $dbConnectionService
     */
    public function __construct(DbConnectionContract $dbConnectionService)
    {
        $this->dbConnectionService = $dbConnectionService;
    }

    /**
     * @param $event
     * @throws \ReflectionException
     */
    public function handle($event)
    {
        $database = $event->user->database;
        if (! empty($database)) {
            $this->dbConnectionService->setCrmConnection($database);
        }
    }
}
