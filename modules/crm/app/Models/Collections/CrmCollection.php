<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/3/2020
 * TIME: 11:11 AM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\App\Models\Collections;

use Illuminate\Database\Eloquent\Collection;

class CrmCollection extends Collection
{
    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    #endregion

    #region MAIN METHODS
    #endregion

    #region SERVICE METHODS
    #endregion
}
