<?php

namespace Crm\App\Models\Collections;

use Crm\App\Models\Database;

;

class DatabaseCollection extends CrmCollection
{
    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    #endregion

    #region MAIN METHODS
    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getDatabasesConfigs(): array
    {
        $configs = [];
        /** @var Database $database */
        foreach ($this as $database) {
            $configs[$database->database] = $database->getRepo()->getCrmConnectionConfig();
        }
        return $configs;
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
