<?php

namespace Crm\App\Models\Collections;

use Crm\App\Models\Role;

class RoleCollection extends CrmCollection
{
    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    #endregion

    #region MAIN METHODS
    /**
     * @return bool
     */
    public function hasOwnerRole(): bool
    {
        if (in_array(Role::OWNER_ROLE_ID, $this->pluck('id')->toArray())) {
            return true;
        }
        return false;
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
