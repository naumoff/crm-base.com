<?php

namespace Crm\App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Crm\App\Models\Collections\ConnectionCollection;

class Connection extends CrmModel
{
    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    protected $table = 'connections';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql';
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    /**
     * @return HasMany
     */
    public function databases(): HasMany
    {
        return $this->hasMany(Database::class, 'connection_id', 'id');
    }
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new ConnectionCollection($models);
    }
    #endregion
}
