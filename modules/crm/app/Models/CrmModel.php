<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/2/2020
 * TIME: 10:10 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\App\Models;

use Crm\App\Models\Repositories\CrmRepository;
use Illuminate\Database\Eloquent\Model;

abstract class CrmModel extends Model
{
    #region MAIN METHODS
    /**
     * @return CrmRepository
     * @throws \ReflectionException
     */
    public function getRepo(): CrmRepository
    {
        $function = new \ReflectionClass($this);
        $repositoryName =  'Crm\App\Models\Repositories\\' . $function->getShortName() . 'Repository';
        return new $repositoryName($this);
    }

    /**
     * @return Database|null
     */
    public function getDatabase(): ?Database
    {
        $dbName = config('database.connections.crm.database');
        return Database::where('database', '=', $dbName)->first();
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
