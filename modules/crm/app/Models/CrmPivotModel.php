<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/2/2020
 * TIME: 10:10 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\App\Models;

use Crm\App\Models\Repositories\CrmRepository;
use Illuminate\Database\Eloquent\Relations\Pivot;

abstract class CrmPivotModel extends Pivot
{
    #region MAIN METHODS
    /**
     * @return CrmRepository
     * @throws \ReflectionException
     */
    public function getRepo(): CrmRepository
    {
        $function = new \ReflectionClass($this);
        $repositoryName =  'Crm\App\Models\Repositories\\' . $function->getShortName() . 'Repository';
        return new $repositoryName($this);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
