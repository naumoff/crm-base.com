<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\CrmUserCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class CrmUser extends CrmModel
{
    #region CLASS CONSTANTS
    public const STATUS_INVITED = 'invited';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_SUSPENDED = 'suspended';
    public const STATUS_REMOVED = 'removed';
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'users';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    #endregion

    #region MAIN METHODS
    /**
     * @return array
     */
    public static function getAvailableStatuses(): array
    {
        return [
            self::STATUS_INVITED,
            self::STATUS_ACTIVE,
            self::STATUS_SUSPENDED,
            self::STATUS_REMOVED,
        ];
    }
    #endregion

    #region SCOPE METHODS
    #endregion

    #region ACCESSORS AND MUTATORS
    #endregion

    #region RELATION METHODS
    /**
     * @return BelongsToMany
     */
    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(Group::class, 'group_members', 'user_id', 'group_id')
            ->using(GroupMember::class);
    }

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'group_members', 'user_id', 'role_id')
            ->using(GroupMember::class);
    }

    /**
     * @return BelongsTo
     */
    public function currentGroup(): BelongsTo
    {
        return $this->belongsTo(Group::class, 'current_group_id', 'id');
    }
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new CrmUserCollection($models);
    }
    #endregion
}
