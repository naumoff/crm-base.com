<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\DatabaseCollection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Database extends CrmModel
{
    use SoftDeletes;

    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    protected $table = 'databases';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql';
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    /**
     * @return BelongsTo
     */
    public function connection(): BelongsTo
    {
        return $this->belongsTo(Connection::class, 'connection_id', 'id');
    }
    #endregion

    #region REPOSITORY METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new DatabaseCollection($models);
    }
    #endregion
}
