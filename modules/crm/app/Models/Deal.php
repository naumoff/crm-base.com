<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\DealCollection;
use Crm\app\Models\Helpers\EnumPriority;
use Crm\app\Models\Helpers\EnumPriorityHelper;

class Deal extends CrmModel implements EnumPriority
{
    use EnumPriorityHelper;

    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'deals';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new DealCollection($models);
    }
    #endregion
}
