<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\GroupCollection;
use Crm\App\Models\Helpers\EnumStatusWithSolid;
use Crm\app\Models\Helpers\EnumStatusWithSolidHelper;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Group extends CrmModel implements EnumStatusWithSolid
{
    use EnumStatusWithSolidHelper;

    #region CLASS CONSTANTS
    public const COMPANY_ID = 1;
    public const COMPANY_NAME = 'COMPANY';
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'groups';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    #endregion

    #region MAIN METHODS

    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->BelongsToMany(CrmUser::class, 'group_members', 'group_id', 'user_id')
            ->using(GroupMember::class);
    }
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new GroupCollection($models);
    }
    #endregion
}
