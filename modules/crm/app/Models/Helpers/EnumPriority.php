<?php

namespace Crm\app\Models\Helpers;

interface EnumPriority
{
    public const PRIORITY_HIGH = 'high';
    public const PRIORITY_MEDIUM = 'medium';
    public const PRIORITY_LOW = 'low';

    public static function getAvailablePriorities(): array;
}
