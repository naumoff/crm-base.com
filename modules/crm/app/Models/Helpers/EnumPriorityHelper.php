<?php

namespace Crm\app\Models\Helpers;

trait EnumPriorityHelper
{
    /**
     * @return array
     */
    public static function getAvailablePriorities(): array
    {
        return [
            EnumPriority::PRIORITY_HIGH,
            EnumPriority::PRIORITY_MEDIUM,
            EnumPriority::PRIORITY_LOW,
        ];
    }
}
