<?php

namespace Crm\App\Models\Helpers;

interface EnumStatus
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_SUSPENDED = 'suspended';
    public const STATUS_REMOVED = 'removed';

    public static function getAvailableStatuses(): array;
}
