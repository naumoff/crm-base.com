<?php

namespace Crm\app\Models\Helpers;

trait EnumStatusHelper
{
    /**
     * @param bool $withSolid
     * @return array
     */
    public static function getAvailableStatuses(): array
    {
        return [
            EnumStatus::STATUS_ACTIVE,
            EnumStatus::STATUS_SUSPENDED,
            EnumStatus::STATUS_REMOVED,
        ];
    }
}
