<?php

namespace Crm\App\Models\Helpers;

interface EnumStatusWithSolid
{
    public const STATUS_SOLID = 'solid';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_SUSPENDED = 'suspended';
    public const STATUS_REMOVED = 'removed';

    public static function getAvailableStatuses(bool $withSolid = true): array;
}
