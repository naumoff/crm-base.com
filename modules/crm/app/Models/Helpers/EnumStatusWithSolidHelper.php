<?php

namespace Crm\app\Models\Helpers;

trait EnumStatusWithSolidHelper
{
    /**
     * @param bool $withSolid
     * @return array
     */
    public static function getAvailableStatuses(bool $withSolid = true): array
    {
        $result = [
            EnumStatusWithSolid::STATUS_ACTIVE,
            EnumStatusWithSolid::STATUS_SUSPENDED,
            EnumStatusWithSolid::STATUS_REMOVED,
        ];

        if ($withSolid) {
            $result[] = EnumStatusWithSolid::STATUS_SOLID;
        }

        return $result;
    }
}
