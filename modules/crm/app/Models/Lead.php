<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\LeadCollection;
use Crm\app\Models\Helpers\EnumPriority;
use Crm\app\Models\Helpers\EnumPriorityHelper;

class Lead extends CrmModel implements EnumPriority
{
    use EnumPriorityHelper;

    #region CLASS CONSTANTS
    public const STATUS_OPEN = 'open';
    public const STATUS_DISQUALIFIED = 'disqualified';
    public const STATUS_CONVERTED = 'converted';
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'leads';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    #endregion

    #region MAIN METHODS
    /**
     * @return array
     */
    public static function getAvailableStatuses(): array
    {
        return [
            self::STATUS_OPEN,
            self::STATUS_CONVERTED,
            self::STATUS_DISQUALIFIED,
        ];
    }
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new LeadCollection($models);
    }
    #endregion
}
