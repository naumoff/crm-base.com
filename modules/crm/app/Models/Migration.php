<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\MigrationCollection;

class Migration extends CrmModel
{
    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'migrations';
    protected $guarded = [];
    public $timestamps = false;
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion

    #region REPOSITORY METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new MigrationCollection($models);
    }
    #endregion
}
