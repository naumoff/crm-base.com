<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\PersonCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends CrmModel
{
    use SoftDeletes;

    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'people';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new PersonCollection($models);
    }
    #endregion
}
