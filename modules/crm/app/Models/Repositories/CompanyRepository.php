<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Company;

class CompanyRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(Company $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
