<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/2/2020
 * TIME: 9:13 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Connection;
use Crm\App\Models\Database;

class ConnectionRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN
    /**
     * ConnectionRepository constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        parent::__construct($connection);
    }

    /**
     * @return Connection
     */
    public function getFreeConnection(): Connection
    {
        return Connection::whereRaw('db_qty = (SELECT MIN(db_qty) FROM connections)')->first();
    }

    /**
     * @return array
     */
    public function getConnectionData(): array
    {
        $dbConnection = $this->model->connection;
        $host = $this->model->host;
        $port = $this->model->port;
        $username = $this->model->username;
        $password = $this->model->password;
        return [$dbConnection, $host, $port, $username, $password];
    }

    /**
     * @param string $newDbName
     * @return Database|null
     */
    public function recordNewDb(string $newDbName): ?Database
    {
        try {
            $this->model->db_qty += 1;
            $this->model->save();
            $database = new Database();
            $database->database = $newDbName;
            $this->model->databases()->save($database);
            return $database;
        } catch (\Throwable $throwable) {
            return null;
        }
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
