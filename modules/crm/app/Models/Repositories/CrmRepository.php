<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/2/2020
 * TIME: 9:36 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\App\Models\Repositories;


use Illuminate\Database\Eloquent\Model;

class CrmRepository
{
    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->model->getConnection()->reconnect();
    }

    /**
     * @param string $increment
     * @return Model|null
     */
    public function getLastInsert($increment = 'id'): ?Model
    {
        return $this->model->orderBy($increment, 'DESC')->first();
    }
}
