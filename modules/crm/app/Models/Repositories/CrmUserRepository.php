<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\CrmUser;

class CrmUserRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(CrmUser $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region MAIN METHODS
    /**
     * @return bool
     */
    public function isOwner(): bool
    {
        return $this->model->roles->hasOwnerRole();
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
