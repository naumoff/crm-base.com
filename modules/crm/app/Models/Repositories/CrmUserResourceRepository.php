<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\CrmUserResource;

class CrmUserResourceRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(CrmUserResource $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
