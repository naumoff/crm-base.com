<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/2/2020
 * TIME: 9:34 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Database;

class DatabaseRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(Database $database)
    {
        parent::__construct($database);
    }

    /**
     * @return array
     */
    public function getCrmConnectionConfig(): array
    {
        /** Connection */
        $connection = $this->model->connection;
        return [
            'database.default' => 'crm',
            'database.connections.crm.driver' => 'mysql',
            'database.connections.crm.host' => $connection->host,
            'database.connections.crm.port' =>  $connection->port,
            'database.connections.crm.database' => $this->model->database,
            'database.connections.crm.username' => $connection->username,
            'database.connections.crm.password' => $connection->password,
        ];
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
