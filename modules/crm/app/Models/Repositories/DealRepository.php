<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Deal;

class DealRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(Deal $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
