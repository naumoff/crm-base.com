<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\GroupMember;

class GroupMemberRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(GroupMember $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
