<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Group;
use Illuminate\Database\Eloquent\Model;

class GroupRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(Group $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Group|null
     */
    public function getCompanyGroup(): ?Group
    {
        return $this->model->find(Group::COMPANY_ID);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
