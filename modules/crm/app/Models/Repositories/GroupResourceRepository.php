<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\GroupResource;

class GroupResourceRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(GroupResource $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
