<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Lead;

class LeadRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(Lead $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
