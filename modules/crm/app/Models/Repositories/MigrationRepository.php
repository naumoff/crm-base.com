<?php

namespace Crm\App\Models\Repositories;;

use Crm\App\Models\Migration;
use Illuminate\Support\Facades\Log;

class MigrationRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(Migration $model)
    {
        parent::__construct($model);
    }

    /**
     * @return int
     */
    public function getNextBatchId(): int
    {
        $lastMigration = $this->model->latest('id')->first();
        if ($lastMigration) {
            return $lastMigration->batch + 1;
        } else {
            return 1;
        }
    }

    /**
     * @param array $existingMigrations
     * @param int|null $step
     * @return array
     */
    public function getUndoneMigrations(array $existingMigrations, ?int $step): array
    {
        $doneMigrations = $this->model->all(['migration'])->pluck('migration')->toArray();
        $undoneMigrations = [];
        foreach ($existingMigrations as $existingMigration) {
            if (!in_array(basename($existingMigration, '.php'), $doneMigrations)) {
                $undoneMigrations[] = $existingMigration;
            }
        }
        if (!empty($step)) {
            $undoneMigrations = array_slice($undoneMigrations, 0, $step);
        }
        return $undoneMigrations;
    }

    /**
     * @param int|null $step
     * @return array
     */
    public function getDoneMigrations(?int $step): array
    {
        $doneMigrations = $this->model->all()->sortBy('batch', SORT_NUMERIC, true)->groupBy('batch');
        $migrationsToRollback = [];
        foreach ($doneMigrations as $migrationCollection) {
            foreach ($migrationCollection->sortBy('id', SORT_NUMERIC, true) as $migration) {
                $migrationsToRollback[] = $migration->migration;
            }
            if (!is_null($step)) {
                $step--;
                if ($step === 0) {
                    break;
                }
            }
        }
        return $migrationsToRollback;
    }

    /**
     * @param string $migrationName
     * @return bool
     * @throws \Exception
     */
    public function deleteMigrationByName(string $migrationName): bool
    {
        /** @var Migration|null $migration */
        $migration = Migration::where('migration', '=', $migrationName)->first();
        if ($migration) {
            $migration->delete();
            return true;
        } else {
            Log::alert("Migration with name {$migrationName} cannot not be found in DB and deleted from DB!");
            return false;
        }
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
