<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Person;

class PersonRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(Person $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
