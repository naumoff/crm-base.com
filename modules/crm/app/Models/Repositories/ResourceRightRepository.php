<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\ResourceRight;

class ResourceRightRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(ResourceRight $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
