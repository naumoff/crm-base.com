<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Collections\CrmCollection;
use Crm\App\Models\ResourceType;
use Illuminate\Support\Str;

class ResourceTypeRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(ResourceType $model)
    {
        parent::__construct($model);
    }

    /**
     * @return CrmCollection|null
     * @throws \ReflectionException
     */
    public function getResources(): ?CrmCollection
    {
        $function = new \ReflectionClass($this->model->name);
        $methodName = Str::plural(Str::camel($function->getShortName()));

        if (method_exists($this->model, $methodName)) {
            return $this->model->$methodName;
        } else {
            return null;
        }
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
