<?php

namespace Crm\App\Models\Repositories;

use Crm\App\Models\Role;

class RoleRepository extends CrmRepository
{
    #region PROPERTIES
    #endregion

    #region MAIN METHODS
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
