<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\ResourceRightCollection;
use Crm\App\Models\Helpers\EnumStatus;
use Crm\app\Models\Helpers\EnumStatusHelper;
use Crm\App\Models\Helpers\EnumStatusWithSolid;
use Crm\app\Models\Helpers\EnumStatusWithSolidHelper;

class ResourceRight extends CrmPivotModel implements EnumStatus
{
    use EnumStatusHelper;

    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'resource_rights';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new ResourceRightCollection($models);
    }
    #endregion
}
