<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\ResourceTypeCollection;
use Crm\App\Models\Helpers\EnumStatusWithSolid;
use Crm\app\Models\Helpers\EnumStatusWithSolidHelper;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ResourceType extends CrmModel implements EnumStatusWithSolid
{
    use EnumStatusWithSolidHelper;

    #region CLASS CONSTANTS
    /** @var array */
    public const RESOURCE_TYPE_LEAD = [
        'id' => 1,
        'name' => Lead::class,
    ];
    /** @var array */
    public const RESOURCE_TYPE_DEAL = [
        'id' => 2,
        'name' => Deal::class,
    ];
    /** @var array */
    public const RESOURCE_TYPE_COMPANY = [
        'id' => 3,
        'name' => Company::class,
    ];
    /** @var array */
    public const RESOURCE_TYPE_PERSON = [
        'id' => 4,
        'name' => Person::class,
    ];

    /** @var string */
    public const ACCESS_PUBLIC = 'public';
    /** @var string */
    public const ACCESS_PRIVATE = 'private';
    /** @var string */
    public const ACCESS_GROUP = 'group';
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'resource_types';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    #endregion

    #region MAIN METHODS
    /**
     * @return array
     */
    public static function getAvailableAccessTypes(): array
    {
        return [
            self::ACCESS_PUBLIC,
            self::ACCESS_PRIVATE,
            self::ACCESS_GROUP,
        ];
    }
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    /**
     * @return HasMany
     */
    public function leads(): HasMany
    {
        return $this->hasMany(Lead::class, 'resource_type_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function people(): HasMany
    {
        return $this->hasMany(Person::class, 'resource_type_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function deals(): HasMany
    {
        return $this->hasMany(Deal::class, 'resource_type_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function companies(): HasMany
    {
        return $this->hasMany(Company::class, 'resource_type_id', 'id');
    }
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new ResourceTypeCollection($models);
    }
    #endregion
}
