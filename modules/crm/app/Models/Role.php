<?php

namespace Crm\App\Models;

use Crm\App\Models\Collections\RoleCollection;
use Crm\App\Models\Helpers\EnumStatusWithSolid;
use Crm\app\Models\Helpers\EnumStatusWithSolidHelper;

class Role extends CrmModel implements EnumStatusWithSolid
{
    use EnumStatusWithSolidHelper;

    #region CLASS CONSTANTS
    public const OWNER_ROLE_ID = 1;
    public const OWNER_ROLE_NAME = 'OWNER';
    #endregion

    #region CLASS PROPERTIES
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'crm';
    protected $table = 'roles';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion

    #region COLLECTION METHOD
    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new RoleCollection($models);
    }
    #endregion
}
