<?php

namespace Crm\App\Policies;

use App\User;
use Crm\App\Models\CrmUser;
use Crm\App\Models\ResourceType;
use Crm\App\Services\Rights\Contracts\ResourceRightsContract;

abstract class CrmResourcePolicy
{
    #region CONSTANTS
    public const CREATE = 'create';
    public const VIEW = 'view';
    public const VIEW_COLLECTION = 'viewCollection';
    public const UPDATE = 'update';
    public const DELETE = 'delete';
    public const RESTORE = 'restore';
    public const SHARE = 'share';
    #endregion

    #region PROPERTIES
    /** @var ResourceRightsContract */
    protected ResourceRightsContract $resourceRightService;
    #endregion

    #region MAIN METHODS
    /**
     * CrmResourcePolicy constructor.
     * @param ResourceRightsContract $resourceRightsContract
     */
    public function __construct(ResourceRightsContract $resourceRightsContract)
    {
        $this->resourceRightService = $resourceRightsContract;
    }

    /**
     * @return ResourceType
     */
    abstract public function getResourceType(): ResourceType;

    /**
     * @param User $user
     * @param string $abiltiy
     * @return bool|void
     * @throws \ReflectionException
     */
    public function before(User $user, string $abiltiy)
    {
        $crmUser = $user->crmUser();

        if ($crmUser === null) {
            dump('user is null');
            return false;
        }

        if ($crmUser->status !== CrmUser::STATUS_ACTIVE) {
            dump('user is not active');
            return false;
        }

        if ($crmUser->getRepo()->isOwner()) {
            dump('user is owner');
            return true;
        }

        $resourceType = $this->getResourceType();
        if ($resourceType->accessType === ResourceType::ACCESS_PUBLIC) {
            dump('resource is public');
            return true;
        }

        $currentGroup = $crmUser->currentGroup ?? null;
        if ($currentGroup === null) {
            dump('no current group');
            return false;
        }

        $this->resourceRightService->setVariables($crmUser, $currentGroup, $resourceType);
    }
    #endregion
}
