<?php

namespace Crm\App\Policies;

use Crm\App\Models\Collections\LeadCollection;
use Crm\App\Models\Lead;
use App\User;
use Crm\App\Models\ResourceType;
use Illuminate\Auth\Access\HandlesAuthorization;

final class LeadResourcePolicy extends CrmResourcePolicy
{
    use HandlesAuthorization;

    #region MAIN METHODS
    /**
     * @return ResourceType
     */
    public function getResourceType(): ResourceType
    {
        return ResourceType::where('name', '=', Lead::class)->first();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $this->resourceRightService->authorizeResourceCreation();
    }

    /**
     * @param User $user
     * @param Lead $lead
     * @return bool
     */
    public function view(User $user, Lead $lead): bool
    {
        return $this->resourceRightService->authorizeResourceAction($lead, self::VIEW);
    }

    /**
     * @param User $user
     * @param LeadCollection $leadCollection
     * @return bool
     */
    public function viewCollection(User $user, LeadCollection $leadCollection): bool
    {
        return $this->resourceRightService->authorizeResourceCollectionView($leadCollection);
    }

    /**
     * @param User $user
     * @param Lead $lead
     * @return bool
     */
    public function update(User $user, Lead $lead): bool
    {
        return $this->resourceRightService->authorizeResourceAction($lead, self::UPDATE);
    }

    /**
     * @param User $user
     * @param Lead $lead
     * @return bool
     */
    public function delete(User $user, Lead $lead): bool
    {
        return $this->resourceRightService->authorizeResourceAction($lead, self::DELETE);
    }

    /**
     * @param User $user
     * @param Lead $lead
     * @return bool
     */
    public function restore(User $user, Lead $lead): bool
    {
        return $this->resourceRightService->authorizeResourceAction($lead, self::RESTORE);
    }

    /**
     * @param User $user
     * @param Lead $lead
     * @return bool
     */
    public function share(User $user, Lead $lead): bool
    {
        $this->resourceRightService->authorizeResourceAction($lead, self::SHARE);
    }
    #endregion
}
