<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/1/2020
 * TIME: 6:52 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\App\Services\Multitenancy\Contracts;

use Crm\App\Models\Connection;
use Crm\App\Models\Database;
use Crm\App\Services\Multitenancy\DbConnectionService;

/**
 * Interface DbConnectionContract
 * @package Crm\App\Services\Multitenancy\Contracts
 * @see DbConnectionService
 */
interface DbConnectionContract
{
    /**
     * @return Connection|null
     */
    public function getFreeConnection(): ?Connection;

    /**
     * @param Database $database
     * @throws \ReflectionException
     */
    public function setCrmConnection(Database $database): void;
}
