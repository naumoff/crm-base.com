<?php

namespace Crm\App\Services\Multitenancy\Contracts;

use Crm\App\Models\Connection;
use Crm\App\Services\Multitenancy\DbCreationService;

/**
 * Interface DbCreationContract
 * @package Crm\App\Services\Multitenancy\Contracts
 * @see DbCreationService
 */
interface DbCreationContract
{
    /**
     * @param Connection $freeConnection
     * @return string|null
     * @throws \ReflectionException
     */
    public function createNewDb(Connection $freeConnection): ?string;
}
