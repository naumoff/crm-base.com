<?php

namespace Crm\App\Services\Multitenancy\Contracts;

use Crm\App\Services\Multitenancy\DbMigrationService;

/**
 * Interface DbMigrationContract
 * @package Crm\App\Services\Multitenancy\Contracts
 * @see DbMigrationService
 */
interface DbMigrationContract
{
    /**
     * @param int|null $connectId
     * @param int|null $dbId
     * @param int|null $step
     * @return array
     */
    public function migrate(int $connectId = null, int $dbId = null, int $step = null): array;
}
