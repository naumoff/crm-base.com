<?php

namespace Crm\App\Services\Multitenancy\Contracts;

use Crm\App\Services\Multitenancy\DbRollbackService;

/**
 * Interface DbRollbackContract
 * @package Crm\App\Services\Multitenancy\Contracts
 * @see DbRollbackService
 */
interface DbRollbackContract
{
    /**
     * @param int|null $connectId
     * @param int|null $dbId
     * @param int|null $step
     * @return array
     */
    public function rollback(int $connectId = null, int $dbId = null, int $step = null): array;
}
