<?php

namespace Crm\App\Services\Multitenancy\Contracts;

use Crm\App\Services\Multitenancy\DbSeederService;

/**
 * Interface DbSeederContract
 * @package Crm\App\Services\Multitenancy\Contracts\
 * @see DbSeederService
 */
interface DbSeederContract
{
    /**
     * @param int $databaseId
     * @param string|null $seederClassName
     * @return array
     */
    public function seed(int $databaseId, ?string $seederClassName): array;
}
