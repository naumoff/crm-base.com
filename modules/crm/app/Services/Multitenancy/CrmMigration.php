<?php

namespace Crm\App\Services\Multitenancy;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

abstract class CrmMigration extends Migration
{
    /**
     * CrmMigration constructor.
     */
    public function __construct()
    {
        Schema::connection('crm')->getConnection()->reconnect();
    }

    /**
     * @param Blueprint $table
     */
    protected function trackUserActivity (Blueprint $table): void
    {
        $table->unsignedBigInteger('created_by')->nullable();
        $table->unsignedBigInteger('updated_by')->nullable();
        $table->unsignedBigInteger('deleted_by')->nullable();
        $table->foreign('created_by')->references('id')->on('users');
        $table->foreign('updated_by')->references('id')->on('users');
        $table->foreign('deleted_by')->references('id')->on('users');
    }
}
