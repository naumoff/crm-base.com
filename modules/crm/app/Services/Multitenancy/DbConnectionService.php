<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/1/2020
 * TIME: 6:51 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\App\Services\Multitenancy;

use Crm\App\Models\Connection;
use Crm\App\Models\Database;
use Crm\App\Models\Repositories\ConnectionRepository;
use Crm\App\Services\Multitenancy\Contracts\DbConnectionContract;

class DbConnectionService implements DbConnectionContract
{
    #region PROPERTIES
    /** @var ConnectionRepository */
    private ConnectionRepository $connectionRepository;
    #endregion

    #region MAIN METHODS
    /**
     * DbConnectionService constructor.
     * @param ConnectionRepository $connectionRepository
     */
    public function __construct(ConnectionRepository $connectionRepository)
    {
        $this->connectionRepository = $connectionRepository;
    }

    /**
     * @return Connection|null
     */
    public function getFreeConnection(): ?Connection
    {
        return $this->connectionRepository->getFreeConnection();
    }

    /**
     * @param Database $database
     * @throws \ReflectionException
     */
    public function setCrmConnection(Database $database): void
    {
        $crmConnectionConfig = $database->getRepo()->getCrmConnectionConfig();
        config($crmConnectionConfig);
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
