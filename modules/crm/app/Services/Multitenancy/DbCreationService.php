<?php

namespace Crm\App\Services\Multitenancy;

use Crm\App\Models\Connection;
use Crm\App\Models\Database;
use Crm\App\Models\Repositories\DatabaseRepository;
use Crm\App\Services\Multitenancy\Contracts\DbConnectionContract;
use Crm\App\Services\Multitenancy\Contracts\DbCreationContract;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use PDO;

class DbCreationService implements DbCreationContract
{
    #region PROPERTIES
    /** @var DatabaseRepository */
    private DatabaseRepository $databaseRepository;

    /** @var DbConnectionContract */
    private DbConnectionContract $dbConnectionService;
    #endregion

    #region MAIN METHODS
    /**
     * DbCreationService constructor.
     * @param DatabaseRepository $databaseRepository
     * @param DbConnectionContract $dbConnectionService
     */
    public function __construct(DatabaseRepository $databaseRepository, DbConnectionContract $dbConnectionService)
    {
        $this->databaseRepository = $databaseRepository;
        $this->dbConnectionService = $dbConnectionService;
    }

    /**
     * @param Connection $freeConnection
     * @return string|null
     * @throws \ReflectionException
     */
    public function createNewDb(Connection $freeConnection): ?string
    {
        [$driver, $host, $port, $username, $password] = $freeConnection->getRepo()->getConnectionData();
        $pdo = new PDO("{$driver}:host={$host}", $username, $password);
        $dbName = $this->getNewDbName();
        $pdo->query("CREATE DATABASE ".$dbName);
        /** @var Database|null $database */
        $database = $freeConnection->getRepo()->recordNewDb($dbName);
        if ($database) {
            $this->addMigrationTable($database);
            return $dbName;
        }
        return null;
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @return string
     */
    private function getNewDbName(): string
    {
        $lastDatabase = $this->databaseRepository->getLastInsert();
        if ($lastDatabase === null) {
            return '1_'.now()->timestamp;
        } else {
            return $lastDatabase->id + 1 . '_' . now()->timestamp;
        }
    }

    /**
     * @param Database $database
     * @throws \ReflectionException
     */
    private function addMigrationTable(Database $database): void
    {
        $this->dbConnectionService->setCrmConnection($database);
        Schema::connection('crm')->create('migrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('migration');
            $table->integer('batch');
        });
    }
    #endregion
}
