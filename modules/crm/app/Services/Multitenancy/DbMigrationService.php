<?php

namespace Crm\App\Services\Multitenancy;

use Crm\app\Console\Commands\CrmCommand;
use Crm\App\Models\Database;
use Crm\App\Models\Migration;
use Crm\App\Services\Multitenancy\Contracts\DbConnectionContract;
use Crm\App\Services\Multitenancy\Contracts\DbMigrationContract;
use Crm\App\Services\Multitenancy\Helpers\DbMigrationHelper;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class DbMigrationService implements DbMigrationContract
{
    use DbMigrationHelper;

    #region PROPERTIES
    /** @var DbConnectionContract */
    private DbConnectionContract $dbConnectionService;

    /** @var Filesystem */
    private Filesystem $filesystem;

    /** @var array $processedMigrations */
    private array $processedMigrations = [];

    /** @var array $migrationReport */
    private array $migrationReport = [];
    #endregion

    #region MAIN METHODS
    /**
     * DbMigrationService constructor.
     * @param DbConnectionContract $dbConnectionService
     * @param Filesystem $filesystem
     */
    public function __construct(DbConnectionContract $dbConnectionService, Filesystem $filesystem)
    {
        $this->dbConnectionService = $dbConnectionService;
        $this->filesystem = $filesystem;
    }

    /**
     * @param int|null $connectId
     * @param int|null $dbId
     * @param int|null $step
     * @return array
     * @throws \ReflectionException
     */
    public function migrate(int $connectId = null, int $dbId = null, int $step = null): array
    {
        $databasesCollection = $this->getRequiredDatabases($connectId, $dbId);
        if (!empty($databasesCollection) && count($databasesCollection) > 0) {
            // get list of migrations files in database/crm folder
            $migrationsFiles = $this->getCrmMigrationsFiles($this->filesystem);
            foreach ($migrationsFiles as $migrationsFile) {
                require_once $migrationsFile;
            }
        } else {
            $this->migrationReport[CrmCommand::ALERT] = ['no database for migration found!'];
        }

        /** @var Database $database */
        foreach ($databasesCollection ?? [] as $database) {
            $this->dbConnectionService->setCrmConnection($database);
            // go to migrations table and get undone migrations
            $undoneMigrations = $this->getUndoneMigrations($migrationsFiles, $step);
            // migrate undone migrations
            $allFilesMigrated = $this->runMigrations($undoneMigrations, $database);
            // log undone migrations as done in migrations table including butch column
            $this->logProcessedMigrations();
            // break continuation with first problem
            if ($allFilesMigrated !== true) {
                $this->migrationReport[CrmCommand::ALERT][] = 'migration interrupted with error!';
                return $this->migrationReport;
            } else {
                $this->migrationReport[CrmCommand::INFO][] = '********** DB migrated! **********';
            }
        }
        return $this->migrationReport;
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param array $migrationFiles
     * @param int|null $step
     * @return array
     * @throws \ReflectionException
     */
    private function getUndoneMigrations(array $migrationFiles, ?int $step): array
    {
        return (new Migration())->getRepo()->getUndoneMigrations($migrationFiles, $step);
    }

    /**
     * @param array $migrationFiles
     * @param Database $database
     * @return bool
     */
    private function runMigrations(array $migrationFiles, Database $database): bool
    {
        if (count($migrationFiles) === 0) {
            $this->migrationReport[CrmCommand::INFO][] = "DB: {$database->database} has no migrations to migrate";
        }
        foreach ($migrationFiles as $migrationFile) {
            try {
                $migrationClassName = $this->getMigrationClassName($migrationFile);
                /** @var Migration $migrationObject */
                $migrationObject = new $migrationClassName();
                $migrationObject->up();
                $this->processedMigrations[] = $migrationFile;
                $this->migrationReport[CrmCommand::INFO][] = "DB: {$database->database}, migration: " .
                    "{$migrationClassName} - DONE!";
            } catch (\Throwable $throwable) {
                Log::error($throwable->getMessage());
                $this->migrationReport[CrmCommand::ERROR][] = "DB: {$database->database}, migration: " .
                    "{$migrationClassName} - FAILED!";
                $this->migrationReport[CrmCommand::ERROR][] = $throwable->getMessage();
                return false;
            }
        }
        return true;
    }

    /**
     * @throws \ReflectionException
     */
    private function logProcessedMigrations(): void
    {
        $nextBatchId = (new Migration())->getRepo()->getNextBatchId();
        foreach ($this->processedMigrations as $processedMigration) {
            $migration = new Migration();
            $migration->migration = basename($processedMigration, '.php');
            $migration->batch = $nextBatchId;
            $migration->save();
        }
        $this->processedMigrations = [];
    }
    #endregion
}
