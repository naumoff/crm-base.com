<?php

namespace Crm\App\Services\Multitenancy;

use Crm\app\Console\Commands\CrmCommand;
use Crm\App\Models\Database;
use Crm\App\Models\Migration;
use Crm\App\Services\Multitenancy\Contracts\DbConnectionContract;
use Crm\App\Services\Multitenancy\Contracts\DbRollbackContract;
use Crm\App\Services\Multitenancy\Helpers\DbMigrationHelper;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Log;

class DbRollbackService implements DbRollbackContract
{
    use DbMigrationHelper;

    #region PROPERTIES
    /** @var DbConnectionContract */
    private DbConnectionContract $dbConnectionService;

    /** @var Filesystem */
    private Filesystem $filesystem;

    /** @var array $processedMigrations */
    private array $rollBackedMigrations = [];

    /** @var array $rollbackReport */
    private array $rollbackReport = [];
    #endregion

    #region MAIN METHODS
    /**
     * DbMigrationService constructor.
     * @param DbConnectionContract $dbConnectionService
     * @param Filesystem $filesystem
     */
    public function __construct(DbConnectionContract $dbConnectionService, Filesystem $filesystem)
    {
        $this->dbConnectionService = $dbConnectionService;
        $this->filesystem = $filesystem;
    }

    /**
     * @param int|null $connectId
     * @param int|null $dbId
     * @param int|null $step
     * @return array
     */
    public function rollback(int $connectId = null, int $dbId = null, int $step = null): array
    {
        $databasesCollection = $this->getRequiredDatabases($connectId, $dbId);
        if (!empty($databasesCollection) && count($databasesCollection) > 0) {
            // get list of migrations files in database/crm folder
            $migrationsFiles = $this->getCrmMigrationsFiles($this->filesystem);
            foreach ($migrationsFiles as $migrationsFile) {
                require_once $migrationsFile;
            }
        } else {
            $this->rollbackReport[CrmCommand::ALERT] = ['no database for migration rollback found!'];
        }

        /** @var Database $database */
        foreach ($databasesCollection ?? [] as $database) {
            $this->dbConnectionService->setCrmConnection($database);
            // go to migrations table and get done migrations
            $doneMigrations = $this->getDoneMigrations($step);
            // rollback done migrations
            $allFilesMigrated = $this->runRollback($doneMigrations, $database);
            // log undone migrations as done in migrations table including butch column
            $allFilesLogged = $this->logRollBackedMigrations();

            // break continuation with first problem
            if ($allFilesMigrated !== true || $allFilesLogged !== true) {
                $this->rollbackReport[CrmCommand::ALERT][] = 'migration interrupted with error!';
                break;
            } else {
                $this->rollbackReport[CrmCommand::INFO][] = '********** DB roll backed! **********';
            }
        }
        return $this->rollbackReport;
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param int|null $step
     * @return array
     * @throws \ReflectionException
     */
    private function getDoneMigrations(?int $step): array
    {
        return (new Migration())->getRepo()->getDoneMigrations($step);
    }

    /**
     * @param array $migrationFiles
     * @param Database $database
     * @return bool
     */
    private function runRollback(array $migrationFiles, Database $database): bool
    {
        if (count($migrationFiles) === 0) {
            $this->rollbackReport[CrmCommand::INFO][] = "DB: {$database->database} has no migrations to rollback";
        }

        foreach ($migrationFiles as $migrationFile) {
            try {
                $migrationClassName = $this->getMigrationClassName($migrationFile);
                /** @var Migration $migrationObject */
                $migrationObject = new $migrationClassName();
                $migrationObject->down();
                $this->rollBackedMigrations[] = $migrationFile;
                $this->rollbackReport[CrmCommand::INFO][] = "DB: {$database->database}, rollback: " .
                    "{$migrationClassName} - DONE!";
            } catch (\Throwable $throwable) {
                Log::error($throwable->getMessage());
                $this->rollbackReport[CrmCommand::ERROR][] = "DB: {$database->database}, rollback: " .
                    "{$migrationClassName} - FAILED!";
                $this->rollbackReport[CrmCommand::ERROR][] = $throwable->getMessage();
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool
     * @throws \ReflectionException
     */
    private function logRollBackedMigrations(): bool
    {
        foreach ($this->rollBackedMigrations as $rollBackedMigration) {
            $deletionStatus = (new Migration())->getRepo()->deleteMigrationByName($rollBackedMigration);
            if ($deletionStatus === false) {
                $this->rollbackReport[CrmCommand::ERROR] = "System cannot find in DB roll backed migration " .
                    "{$rollBackedMigration}!";
                return false;
            }
        }
        $this->rollBackedMigrations = [];
        return true;
    }
    #endregion
}
