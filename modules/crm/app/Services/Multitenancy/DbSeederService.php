<?php

namespace Crm\App\Services\Multitenancy;

use Crm\app\Console\Commands\CrmCommand;
use Crm\App\Models\Database;
use Crm\App\Services\Multitenancy\Contracts\DbConnectionContract;
use Crm\App\Services\Multitenancy\Contracts\DbSeederContract;
use Crm\Database\Seeds\CrmCentralSeeder;
use Crm\database\seeds\CrmSeeder;

class DbSeederService implements DbSeederContract
{
    #region PROPERTIES
    /** @var DbConnectionContract */
    private DbConnectionContract $dbConnectionService;

    /** @var array $seederReport */
    private array $seederReport = [];
    #endregion

    #region MAIN METHODS
    /**
     * DbSeederService constructor.
     * @param DbConnectionContract $connectionContract
     */
    public function __construct(DbConnectionContract $connectionContract)
    {
        $this->dbConnectionService = $connectionContract;
    }

    /**
     * @param int $databaseId
     * @param string|null $seederClassName
     * @return array
     * @throws \ReflectionException
     */
    public function seed(int $databaseId, ?string $seederClassName): array
    {
        $database = Database::find($databaseId);
        if (empty($database)) {
            $this->seederReport[CrmCommand::ERROR] = ['No database found for seeding!'];
            return $this->seederReport;
        }

        if (config('app.env') === 'production') {
            $this->seederReport[CrmCommand::ERROR] = ['No seeders on live DB allowed!'];
            return $this->seederReport;
        }

        $this->dbConnectionService->setCrmConnection($database);
        if (empty($seederClassName)) {
            $this->runAllCrmSeeders();
        } else {
            if (empty($seeder = $this->getPreciseSeeder($seederClassName))) {
                return $this->seederReport;
            }
            $this->runPreciseCrmSeeder($seeder);
        }
        return $this->seederReport;
    }
    #endregion

    #region SERVICE METHODS
    private function runAllCrmSeeders(): void
    {
        $crmCentralSeeder = new CrmCentralSeeder();
        $crmCentralSeeder->run();
        $this->seederReport[CrmCommand::INFO] = $crmCentralSeeder->getCompletionMessages();
        if ($crmCentralSeeder->isCompleted() === true) {
            $this->seederReport[CrmCommand::INFO][] = 'DB seeded successfully!';
        } else {
            $this->seederReport[CrmCommand::ALERT] = $crmCentralSeeder->getErrorMessages();
            $this->seederReport[CrmCommand::ERROR] = ['DB seeding FAILED! Check log files!'];
        }
    }

    /**
     * @param string $seederClassName
     * @return CrmSeeder|null
     */
    private function getPreciseSeeder(string $seederClassName): ?CrmSeeder
    {
        $seederFullName = 'Crm\Database\Seeds\\'.$seederClassName;
        if (class_exists($seederFullName)) {
            $seeder = new $seederFullName();
            if ($seeder instanceof CrmSeeder) {
                return $seeder;
            } else {
                $this->seederReport[CrmCommand::ERROR] = ["Seeder Class Name must be instance of CrmSeeder!"];
            }
        } else {
            $this->seederReport[CrmCommand::ERROR] = ["Seeder Class Name {$seederFullName} doesn't exist!"];
        }
        return null;
    }

    /**
     * @param CrmSeeder $crmSeeder
     */
    private function runPreciseCrmSeeder(CrmSeeder $crmSeeder): void
    {
        $crmSeeder->run();
        if ($crmSeeder->isCompleted() === true) {
            $crmSeederClassName = get_class($crmSeeder);
            $this->seederReport[CrmCommand::INFO] = ["DB seeder {$crmSeederClassName} run successfully!"];
        } else {
            $this->seederReport[CrmCommand::ERROR] = ['DB seeding FAILED! Check log file!'];
        }
    }
    #endregion
}
