<?php

namespace Crm\App\Services\Multitenancy\Helpers;

use Crm\App\Models\Collections\DatabaseCollection;
use Crm\App\Models\Connection;
use Crm\App\Models\Database;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

trait DbMigrationHelper
{
    /**
     * @param int|null $connectId
     * @param int|null $dbId
     * @return DatabaseCollection|null
     */
    private function getRequiredDatabases(int $connectId = null, int $dbId = null): ?DatabaseCollection
    {
        // we migrate all crm databases
        if ($connectId === null && $dbId === null) {
            /** @var DatabaseCollection $databases */
            return Database::all();
        } elseif (is_int($dbId)) {
            return Database::where('id', $dbId)->get();
        } elseif (is_int($connectId)) {
            return Connection::find($connectId)->databases ?? null;
        }
        return null;
    }

    /**
     * @return array
     */
    private function getCrmMigrationsFiles(Filesystem $filesystem): array
    {
        $path = __DIR__.'/../../../../database/migrations/crm';
        $migrationFiles = $filesystem->glob($path.'/*.php');
        return $migrationFiles;
    }

    /**
     * @param string $migrationPath
     * @return string
     */
    private function getMigrationClassName(string $migrationPath): string
    {
        $className = basename($migrationPath, '.php');
        $datePattern = '/\d\d\d\d_\d\d_\d\d_\d\d\d\d\d\d_/';
        $className = preg_replace($datePattern, '', $className);
        return '\\' . Str::studly($className);
    }
}
