<?php

namespace Crm\App\Services\Rights\Contracts;

use Crm\App\Models\Collections\CrmCollection;
use Crm\App\Models\CrmUser;
use Crm\App\Models\Group;
use Crm\App\Models\ResourceType;
use Illuminate\Database\Eloquent\Model;

interface ResourceRightsContract
{
    public function setVariables(CrmUser $crmUser, Group $group, ResourceType $resourceType): void;

    public function authorizeResourceCreation(): bool;

    public function authorizeResourceAction(Model $resource, string $ability): bool;

    public function authorizeResourceCollectionView(CrmCollection $resourceCollection): bool;
}
