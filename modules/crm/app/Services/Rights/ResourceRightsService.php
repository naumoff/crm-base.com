<?php

namespace Crm\App\Services\Rights;

use Crm\App\Models\Collections\CrmCollection;
use Crm\App\Models\CrmUser;
use Crm\App\Models\Group;
use Crm\App\Models\ResourceType;
use Crm\App\Services\Rights\Contracts\ResourceRightsContract;
use Illuminate\Database\Eloquent\Model;

class ResourceRightsService implements ResourceRightsContract
{
    #region PROPERTIES
    private CrmUser $crmUser;
    private Group $currentGroup;
    private ResourceType $resourceType;
    #endregion

    #region MAIN METHODS
    /**
     * @param CrmUser $crmUser
     * @param Group $group
     * @param ResourceType $resourceType
     */
    public function setVariables(CrmUser $crmUser, Group $group, ResourceType $resourceType): void
    {
        $this->crmUser = $crmUser;
        $this->currentGroup = $group;
        $this->resourceType = $resourceType;
    }

    public function authorizeResourceCreation(): bool
    {
        dump('creation');
        return true;
    }

    public function authorizeResourceAction(Model $resource, string $ability): bool
    {
        dump($ability . ' auth');
        // TODO: Implement authorizeResourceAction() method.
        return true;
    }

    public function authorizeResourceCollectionView(CrmCollection $resourceCollection): bool
    {
        dump('collection view auth');
        // TODO: Implement authorizeViewResourceCollection() method.
        return true;
    }
    #endregion

    #region SERVICE METHODS
    #endregion
}
