<?php

/** @var Factory $factory */

use Crm\App\Models\CrmUser;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Crm\App\Models\Company;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'notes' => $faker->text,
        'created_by' => CrmUser::all()->random()->id,
    ];
});
