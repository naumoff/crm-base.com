<?php
/** @var Factory $factory */
use Crm\App\Models\Connection;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Connection::class, function (Faker $faker) {
    return [
        'connection' => 'mysql',
        'host' => '192.168.10.11',
        'port' => '3306',
        'username' => 'homestead',
        'password' => 'secret',
    ];
});
