<?php

use Crm\App\Models\CrmUser;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(CrmUser::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'current_group_id' => null,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'birthday' => $faker->date('Y-m-d'),
        'status' => CrmUser::STATUS_ACTIVE,
        'remember_token' => Str::random(10),
    ];
});

$factory->state(CrmUser::class, CrmUser::STATUS_INVITED, function (Faker $generator) {
    return [
        'email_verified_at' => null,
        'status' => CrmUser::STATUS_INVITED,
    ];
});

$factory->state(CrmUser::class, CrmUser::STATUS_ACTIVE, function (Faker $generator) {
    return [
        'status' => CrmUser::STATUS_ACTIVE,
    ];
});

$factory->state(CrmUser::class, CrmUser::STATUS_SUSPENDED, function (Faker $generator) {
    return [
        'status' => CrmUser::STATUS_SUSPENDED,
    ];
});

$factory->state(CrmUser::class, CrmUser::STATUS_REMOVED, function (Faker $generator) {
    return [
        'status' => CrmUser::STATUS_REMOVED,
    ];
});
