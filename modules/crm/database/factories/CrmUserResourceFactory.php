<?php

use Crm\App\Models\CrmUserResource;
use Crm\App\Models\GroupResource;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(CrmUserResource::class, function (Faker $faker) {
    $groupResource = GroupResource::all()->random();
    $group = $groupResource->group;
    $user = $group->users->random();
    return [
        'user_id' => $user->id,
        'group_id' => $group->id,
        'resource_type_id' => $groupResource->resource_type_id,
        'resource_id' => $groupResource->resource_id,
        'status' => collect(GroupResource::getAvailableStatuses())->random(),
    ];
});
