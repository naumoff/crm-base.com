<?php

use Crm\App\Models\Group;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$groupNames = [
    'b2b sales dep`t',
    'b2c sales dep`t',
    'marketing dep`t',
    'financial dep`t',
    'accounting dep`t',
];

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Group::class, function (Faker $faker) use ($groupNames) {

    return [
        'name' => $groupNames[random_int(0, 4)] . ' (' . uniqid() . ')',
        'status' => Group::getAvailableStatuses(false)[array_rand(Group::getAvailableStatuses(false), 1)],
        'mother_group_id' => Group::COMPANY_ID
    ];
});

$factory->state(Group::class, Group::STATUS_ACTIVE, function (Faker $generator) {
    return [
        'status' => Group::STATUS_ACTIVE,
    ];
});

$factory->state(Group::class, Group::STATUS_SUSPENDED, function (Faker $generator) {
    return [
        'status' => Group::STATUS_SUSPENDED,
    ];
});

$factory->state(Group::class, Group::STATUS_REMOVED, function (Faker $generator) {
    return [
        'status' => Group::STATUS_REMOVED,
    ];
});

$factory->state(Group::class, 'subgroup', function (Faker $generator) use ($groupNames) {
    /** @var \Crm\App\Models\Collections\GroupCollection $groups */
    $groups = Group::where('id', '>', Group::COMPANY_ID)->get();

    if (count($groups)) {
        $randomGroupId = $groups->random()->id;
    } else {
        $randomGroupId = null;
    }

    return [
        'name' => ($randomGroupId)? 'SUB-' . $randomGroupId . ' (' . uniqid() . ')': null,
        'mother_group_id' => $randomGroupId,
    ];
});
