<?php

use Crm\App\Models\CrmUser;
use Crm\App\Models\Group;
use Crm\App\Models\GroupResource;
use Crm\App\Models\ResourceType;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Crm\App\Models\Collections\CrmCollection;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(GroupResource::class, function (Faker $faker) {
    $resourceType = ResourceType::all()->random();
    /** @var CrmCollection | null $resources */
    $resources = $resourceType->getRepo()->getResources();

    if (! empty($resources)) {
        $resource = $resources->random();
    } else {
        $resource = factory($resourceType->name)->create([
            'resource_type_id' => $resourceType->id,
        ]);
    }

    return [
        'group_id' => Group::all()->random()->id,
        'resource_type_id' => $resourceType->id,
        'resource_id' => $resource->id,
        'read' => random_int(0, 1),
        'update' => random_int(0, 1),
        'delete' => random_int(0, 1),
        'share' => random_int(0, 1),
        'status' => collect(GroupResource::getAvailableStatuses())->random(),
        'created_by' => CrmUser::all()->random()->id,
    ];
});

$factory->state(GroupResource::class, 'read+', function (\Faker\Generator $faker) {
    return ['read' => 1];
});

$factory->state(GroupResource::class, 'read-', function (\Faker\Generator $faker) {
    return ['read' => 0];
});

$factory->state(GroupResource::class, 'update+', function (\Faker\Generator $faker) {
    return ['update' => 1];
});

$factory->state(GroupResource::class, 'update-', function (\Faker\Generator $faker) {
    return ['update' => 0];
});

$factory->state(GroupResource::class, 'delete+', function (\Faker\Generator $faker) {
    return ['delete' => 1];
});

$factory->state(GroupResource::class, 'delete-', function (\Faker\Generator $faker) {
    return ['delete' => 0];
});

$factory->state(GroupResource::class, 'share+', function (\Faker\Generator $faker) {
    return ['share' => 1];
});

$factory->state(GroupResource::class, 'share-', function (\Faker\Generator $faker) {
    return ['share' => 0];
});

$factory->state(GroupResource::class, GroupResource::STATUS_ACTIVE, function (\Faker\Generator $faker) {
    return ['status' => GroupResource::STATUS_ACTIVE];
});

$factory->state(GroupResource::class, GroupResource::STATUS_SUSPENDED, function (\Faker\Generator $faker) {
    return ['status' => GroupResource::STATUS_SUSPENDED];
});

$factory->state(GroupResource::class, GroupResource::STATUS_REMOVED, function (\Faker\Generator $faker) {
    return ['status' => GroupResource::STATUS_REMOVED];
});
