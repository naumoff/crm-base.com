<?php

use Crm\App\Models\Lead;
use Crm\App\Models\CrmUser;
use Crm\App\Models\ResourceType;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Lead::class, function (Faker $faker) {
    return [
        'resource_type_id' => ResourceType::RESOURCE_TYPE_LEAD['id'],
        'name' => 'Lead - (' . uniqid() . ')',
        'company_name' => $faker->company,
        'person_name' => $faker->name,
        'notes' => $faker->sentences(2, true),
        'priority' => collect(Lead::getAvailablePriorities())->random(),
        'status' => collect(Lead::getAvailableStatuses())->random(),
        'created_by' => CrmUser::all()->random()->id,
    ];
});

$factory->state(Lead::class, Lead::STATUS_OPEN, function (Faker $generator) {
    return [
       'status' => Lead::STATUS_OPEN,
    ];
});

$factory->state(Lead::class, Lead::STATUS_DISQUALIFIED, function (Faker $generator) {
    return [
        'status' => Lead::STATUS_DISQUALIFIED,
    ];
});

$factory->state(Lead::class, Lead::STATUS_CONVERTED, function (Faker $generator) {
    return [
        'status' => Lead::STATUS_CONVERTED,
    ];
});
