<?php

/** @var Factory $factory */

use Crm\App\Models\CrmUser;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Crm\App\Models\Person;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Person::class, function (Faker $faker) {
    $gender = ['male', 'female'][random_int(0, 1)];
    $firstName = $faker->firstName($gender);
    $lastName = $faker->lastName;

    return [
        'full_name' => $firstName . ' ' . $lastName,
        'first_name' => $firstName,
        'last_name' => $lastName,
        'gender' => $gender[0],
        'notes' => $faker->text(),
        'created_by' => CrmUser::all()->random()->id,
    ];
});
