<?php

use Crm\App\Models\ResourceRight;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Crm\App\Models\Role;
use Crm\App\Models\ResourceType;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(ResourceRight::class, function (Faker $faker) {
    $role = Role::all()->random();
    $resourceType = ResourceType::all()->random();
    return [
        'role_id' => $role->id,
        'resource_type_id' => $resourceType->id,
        'create' => random_int(0, 1),
        'view_own' => random_int(0, 1),
        'view_group' => random_int(0, 1),
        'update_own' => random_int(0, 1),
        'update_group' => random_int(0, 1),
        'delete_own' => random_int(0, 1),
        'delete_group' => random_int(0, 1),
        'restore_own' => random_int(0, 1),
        'restore_group' => random_int(0, 1),
        'share_own' => random_int(0, 1),
        'share_group' => random_int(0, 1),
        'status' => collect(ResourceRight::getAvailableStatuses())->random(),
        'created_by' => null,
        'updated_by' => null,
        'deleted_by' => null,
        'created_at' => now(),
        'updated_at' => now(),
        'deleted_at' => null,
    ];
});
