<?php

use Crm\App\Models\Role;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Role::class, function (Faker $faker) {
    $roles = [
        'director',
        'manager',
        'sales representative',
        'sales agent',
        'assistant'
    ];

    return [
        'name' => $roles[random_int(0, 4)] . ' ('. uniqid() . ')',
        'shared' => true,
        'status' => Role::getAvailableStatuses(false)[array_rand(Role::getAvailableStatuses(false), 1)],
    ];
});

$factory->state(Role::class, Role::STATUS_ACTIVE, function (Faker $generator) {
    return [
        'status' => Role::STATUS_ACTIVE,
    ];
});

$factory->state(Role::class, Role::STATUS_SUSPENDED, function (Faker $generator) {
    return [
        'status' => Role::STATUS_SUSPENDED,
    ];
});

$factory->state(Role::class, Role::STATUS_REMOVED, function (Faker $generator) {
    return [
        'status' => Role::STATUS_REMOVED,
    ];
});

$factory->state(Role::class, 'group_property', function (Faker $generator) {
    return [
        'shared' => false,
    ];
});
