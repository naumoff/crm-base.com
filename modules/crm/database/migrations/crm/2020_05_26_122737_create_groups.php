<?php

use Crm\App\Models\Group;
use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateGroups extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->unsignedBigInteger('mother_group_id')->nullable();
            $table->enum('status', Group::getAvailableStatuses())
                ->comment('group can be suspended or removed by user except solid company group');
            $this->trackUserActivity($table);
            $table->timestamps();
            $table->softDeletes();
        });
        // creating main company group
        $companyGroup = new Group();
        $companyGroup->id = Group::COMPANY_ID;
        $companyGroup->name = Group::COMPANY_NAME;
        $companyGroup->status = Group::STATUS_SOLID;
        $companyGroup->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
