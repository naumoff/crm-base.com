<?php

use Crm\App\Models\Role;
use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoles extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->boolean('shared')->default(false);
            $table->unsignedBigInteger('group_id')->nullable();
            $table->enum('status', Role::getAvailableStatuses())
                ->comment('role can be suspended or removed by user except solid owner role');
            $this->trackUserActivity($table);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('group_id')->references('id')->on('groups')->cascadeOnDelete();
        });
        // creating main owner role
        $role = new Role();
        $role->id = Role::OWNER_ROLE_ID;
        $role->name = Role::OWNER_ROLE_NAME;
        $role->status = Role::STATUS_SOLID;
        $role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
