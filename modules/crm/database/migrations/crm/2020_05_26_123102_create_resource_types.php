<?php

use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Crm\App\Models\ResourceType;

class CreateResourceTypes extends CrmMigration
{
    /** @var array */
    private const RESOURCE_TYPES = [
        ResourceType::RESOURCE_TYPE_LEAD,
        ResourceType::RESOURCE_TYPE_DEAL,
        ResourceType::RESOURCE_TYPE_COMPANY,
        ResourceType::RESOURCE_TYPE_PERSON,
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->enum('access_type', ResourceType::getAvailableAccessTypes());
            $table->enum('status', ResourceType::getAvailableStatuses())
                ->comment('only custom resources can be removed (not solid), but all can be suspended');
            $this->trackUserActivity($table);
            $table->timestamps();
            $table->softDeletes();
        });

        /** @var array $resourceType */
        foreach (self::RESOURCE_TYPES as $resourceObject) {
            $this->createDefaultResourceType($resourceObject);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_types');
    }

    private function createDefaultResourceType(array $resourceObject)
    {
        $resourceType = new ResourceType();
        $resourceType->id = $resourceObject['id'];
        $resourceType->name = $resourceObject['name'];
        $resourceType->access_type = ResourceType::ACCESS_PRIVATE;
        $resourceType->status = ResourceType::STATUS_SOLID;
        $resourceType->save();
    }
}
