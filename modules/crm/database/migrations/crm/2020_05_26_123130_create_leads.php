<?php

use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Crm\App\Models\Lead;

class CreateLeads extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resource_type_id');
            $table->string('name');
            $table->string('company_name')->nullable();
            $table->string('person_name')->nullable();
            $table->longText('notes');
            $table->enum('priority', Lead::getAvailablePriorities())->default(Lead::PRIORITY_LOW);
            $table->enum('status', Lead::getAvailableStatuses())->default(Lead::STATUS_OPEN);
            $this->trackUserActivity($table);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('resource_type_id')->references('id')->on('resource_types')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
