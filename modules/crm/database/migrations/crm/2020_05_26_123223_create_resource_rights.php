<?php

use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Crm\App\Models\ResourceRight;

class CreateResourceRights extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_rights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('resource_type_id');
            $table->boolean('create')->default(false);
            $table->boolean('view_own')->default(false);
            $table->boolean('view_group')->default(false);
            $table->boolean('update_own')->default(false);
            $table->boolean('update_group')->default(false);
            $table->boolean('delete_own')->default(false);
            $table->boolean('delete_group')->default(false);
            $table->boolean('restore_own')->default(false);
            $table->boolean('restore_group')->default(false);
            $table->boolean('share_own')->default(false);
            $table->boolean('share_group')->default(false);
            $table->enum('status', ResourceRight::getAvailableStatuses())
                ->comment('suspended - resource is public or private; removed - role/resource removed');
            $this->trackUserActivity($table);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('role_id')->references('id')->on('roles')->cascadeOnDelete();
            $table->foreign('resource_type_id')->references('id')->on('resource_types')->cascadeOnDelete();
            $table->unique(['role_id', 'resource_type_id'], 'role_resource_type_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_rights');
    }
}
