<?php

use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupResources extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_resources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('resource_type_id');
            $table->unsignedBigInteger('resource_id')
                ->comment('id is relevant to different resources (leads, companies etc)');
            $table->boolean('read');
            $table->boolean('update');
            $table->boolean('delete');
            $table->boolean('share');
            $table->enum('status', \Crm\App\Models\GroupResource::getAvailableStatuses())
                ->comment('removed: group or resource is removed; suspended: group ownership suspended temporary');
            $this->trackUserActivity($table);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('group_id')->references('id')->on('groups')->cascadeOnDelete();
            $table->foreign('resource_type_id')->references('id')->on('resource_types')->cascadeOnDelete();
            $table->unique(['group_id', 'resource_type_id', 'resource_id'], 'group_resource_type_resource_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_resources');
    }
}
