<?php

use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserResources extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_resources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('resource_type_id');
            $table->unsignedBigInteger('resource_id')
                ->comment('id is relevant to different resources (leads, companies etc)');
            $table->enum('status', \Crm\App\Models\CrmUserResource::getAvailableStatuses());
            $this->trackUserActivity($table);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('group_id')->references('id')->on('groups')->cascadeOnDelete();
            $table->foreign('resource_type_id')->references('id')->on('resource_types')->cascadeOnDelete();
            $table->unique(
                ['user_id', 'group_id', 'resource_type_id', 'resource_id'],
                'user_resource_type_resource_unique'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_resources');
    }
}
