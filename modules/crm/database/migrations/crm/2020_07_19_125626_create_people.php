<?php

use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeople extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resource_type_id')
                ->default(\Crm\App\Models\ResourceType::RESOURCE_TYPE_PERSON['id']);
            $table->string('full_name');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->enum('gender', ['m', 'f'])->nullable();
            $table->text('notes');
            $this->trackUserActivity($table);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('resource_type_id')->references('id')->on('resource_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
