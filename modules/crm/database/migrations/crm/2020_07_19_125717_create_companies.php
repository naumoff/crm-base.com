<?php

use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanies extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resource_type_id')
                ->default(\Crm\App\Models\ResourceType::RESOURCE_TYPE_COMPANY['id']);
            $table->string('name');
            $table->text('notes');
            $this->trackUserActivity($table);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('resource_type_id')->references('id')->on('resource_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
