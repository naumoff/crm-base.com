<?php

use Crm\App\Services\Multitenancy\CrmMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Crm\App\Models\Deal;
use Crm\App\Models\ResourceType;

class CreateDeals extends CrmMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resource_type_id')
                ->default(ResourceType::RESOURCE_TYPE_DEAL['id']);
            $table->string('name');
            $table->enum('priority', Deal::getAvailablePriorities());
            $table->text('notes');
            $this->trackUserActivity($table);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('resource_type_id')->references('id')->on('resource_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
