<?php
namespace Crm\Database\Seeds;

use Crm\App\Models\Connection;
use Illuminate\Database\Seeder;

class ConnectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Connection::class)->create();
    }
}
