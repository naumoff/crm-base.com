<?php

namespace Crm\Database\Seeds;

final class CrmCentralSeeder extends CrmSeeder
{
    /** @var array */
    private const CRM_SEEDERS = [
        CrmUserSeeder::class,
        UserSeeder::class,
        CrmRoleSeeder::class,
        CrmGroupSeeder::class,
        CrmGroupMemberSeeder::class,
        CrmLeadSeeder::class,
        CrmResourceRightSeeder::class,
        CrmPersonSeeder::class,
        CrmCompanySeeder::class,
        CrmDealSeeder::class,
        CrmGroupResourceSeeder::class,
        CrmUserResourceSeeder::class,
    ];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        /** @var CrmSeeder $seeder */
        foreach (self::CRM_SEEDERS as $seeder) {
            try {
                $this->call($seeder);
                $this->completionMessages[] = "Seeder {$seeder} completed successfully!";
            } catch (\Throwable $throwable) {
                report($throwable);
                $this->errorMessages[] = "Seeder {$seeder} failed!";
                $this->completionStatus = false;
                break;
            }
        }
    }
}
