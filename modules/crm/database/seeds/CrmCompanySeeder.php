<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\Company;

class CrmCompanySeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Company::class, 20)->create();
    }
}
