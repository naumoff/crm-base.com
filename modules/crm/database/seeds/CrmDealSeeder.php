<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\Deal;

class CrmDealSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Deal::class, 15)->create();
    }
}
