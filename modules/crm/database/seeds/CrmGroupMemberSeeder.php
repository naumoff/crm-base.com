<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\Collections\RoleCollection;
use Crm\App\Models\CrmUser;
use Crm\App\Models\Group;
use Crm\App\Models\GroupMember;
use Crm\App\Models\Role;

class CrmGroupMemberSeeder extends CrmSeeder
{
    /**
     * @throws \Exception
     */
    public function run(): void
    {
        $users = CrmUser::all();
        $groups = Group::all();
        $roles = Role::all();
        if (count($users) && count($roles) && count($groups)) {
            foreach ($users as $user) {
                if ($user->id === 1) {
                    $user->roles()->attach([Role::OWNER_ROLE_ID => [
                        'group_id' => Group::COMPANY_ID
                    ]]);
                    continue;
                }
                foreach ($groups as $group) {
                    $groupMembersLimit = 3;
                    $this->addNewGroupMember($group, $user, $roles, $groupMembersLimit);
                }
            }
        }
    }

    /**
     * @param Group $group
     * @param CrmUser $user
     * @param RoleCollection $roles
     * @param int $groupMembersQuota
     * @throws \Exception
     */
    private function addNewGroupMember(
        Group $group,
        CrmUser $user,
        RoleCollection $roles,
        int &$groupMembersQuota
    ): void {
        if (random_int(0, 1) && $groupMembersQuota) {
            $groupMembersQuota--;
            $newGroupMember = new GroupMember();
            $newGroupMember->user_id = $user->id;
            $newGroupMember->role_id = $roles->random()->id;
            $newGroupMember->group_id = $group->id;
            $newGroupMember->save();
        }
    }
}
