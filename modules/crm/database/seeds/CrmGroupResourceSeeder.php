<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\Group;
use Crm\App\Models\GroupResource;
use Crm\App\Models\ResourceType;

class CrmGroupResourceSeeder extends CrmSeeder
{
    /**
     * @throws \ReflectionException
     */
    public function run(): void
    {
        foreach (Group::all() as $group) {
            /** @var ResourceType $resourceType */
            foreach (ResourceType::all() as $resourceType) {
                $this->setGroupProperty($group, $resourceType);
            }
        }
    }

    /**
     * @param Group $group
     * @param ResourceType $resourceType
     * @throws \ReflectionException
     */
    private function setGroupProperty(Group $group, ResourceType $resourceType): void
    {
        $resources = $resourceType->getRepo()->getResources();
        if (!empty($resources) && count($resources) > 3) {
            $resources = $resources->random(3);

            factory(GroupResource::class)
                ->states([GroupResource::STATUS_ACTIVE, 'read+', 'update+', 'delete+', 'share+'])
                ->create([
                    'group_id' => $group->id,
                    'resource_type_id' => $resourceType->id,
                    'resource_id' => $resources[0]->id,
                ]);

            factory(GroupResource::class)
                ->states([GroupResource::STATUS_ACTIVE, 'read+', 'update-', 'delete-', 'share-'])
                ->create([
                    'group_id' => $group->id,
                    'resource_type_id' => $resourceType->id,
                    'resource_id' => $resources[1]->id,
                ]);

            factory(GroupResource::class)
                ->states([GroupResource::STATUS_SUSPENDED, 'read+', 'update+', 'delete+', 'share+'])
                ->create([
                    'group_id' => $group->id,
                    'resource_type_id' => $resourceType->id,
                    'resource_id' => $resources[2]->id,
                ]);
        }
    }
}
