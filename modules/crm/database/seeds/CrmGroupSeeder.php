<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\Group;

class CrmGroupSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Group::class, 5)->state(Group::STATUS_ACTIVE)->create();
        factory(Group::class, 1)->state(Group::STATUS_SUSPENDED)->create();
        factory(Group::class, 1)->state(Group::STATUS_REMOVED)->create();
        factory(Group::class, 5)->states([Group::STATUS_ACTIVE, 'subgroup'])->create();
        factory(Group::class, 1)->states([Group::STATUS_SUSPENDED, 'subgroup'])->create();
        factory(Group::class, 1)->states([Group::STATUS_REMOVED, 'subgroup'])->create();
    }
}
