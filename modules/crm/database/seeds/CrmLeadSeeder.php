<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\Lead;

class CrmLeadSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Lead::class, 5)->state(Lead::STATUS_OPEN)->create();
        factory(Lead::class, 5)->state(Lead::STATUS_DISQUALIFIED)->create();
        factory(Lead::class, 5)->state(Lead::STATUS_CONVERTED)->create();
    }
}
