<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\Person;

class CrmPersonSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Person::class, 15)->create();
    }
}
