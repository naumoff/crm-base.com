<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\ResourceRight;
use Crm\App\Models\ResourceType;
use Crm\App\Models\Role;

class CrmResourceRightSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $roles = Role::all();
        $resourceTypes = ResourceType::all();
        if (count($roles) && count($resourceTypes)) {
            foreach ($resourceTypes as $resourceType) {
                foreach ($roles as $role) {
                    $this->setRoleRights($role, $resourceType);
                }
            }
        }
    }

    /**
     * @param Role $role
     * @param ResourceType $resourceType
     */
    private function setRoleRights(Role $role, ResourceType $resourceType): void
    {
        factory(ResourceRight::class, 1)->create([
            'role_id' => $role->id,
            'resource_type_id' => $resourceType->id
        ]);
    }
}
