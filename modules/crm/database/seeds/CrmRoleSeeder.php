<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\Role;

final class CrmRoleSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        factory(Role::class, 5)->state(Role::STATUS_ACTIVE)->create();
        factory(Role::class, 5)->states([Role::STATUS_ACTIVE, 'group_property'])->create();
        factory(Role::class, 5)->state(Role::STATUS_SUSPENDED)->create();
        factory(Role::class, 5)->state(Role::STATUS_REMOVED)->create();
    }
}
