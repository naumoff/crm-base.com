<?php

namespace Crm\database\seeds;

use Illuminate\Database\Seeder;

abstract class CrmSeeder extends Seeder
{
    /** @var bool $completionStatus */
    protected bool $completionStatus = true;

    /** @var array $completionMessages */
    protected array $completionMessages = [];

    /** @var array $errorMessages */
    protected array $errorMessages = [];

    /**
     * CRM seeder runner
     */
    abstract public function run(): void;

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completionStatus;
    }

    /**
     * @return array
     */
    public function getCompletionMessages(): array
    {
        return $this->completionMessages;
    }

    public function getErrorMessages(): array
    {
        return $this->errorMessages;
    }
}
