<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\CrmUserResource;
use Crm\App\Models\GroupResource;

class CrmUserResourceSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        foreach (GroupResource::all() as $groupResource) {
            $group = $groupResource->group;
            $user = $group->users->random();
            factory(CrmUserResource::class, 1)->create([
                'user_id' => $user->id,
                'resource_type_id' => $groupResource->resource_type_id,
                'resource_id' => $groupResource->resource_id,
                'group_id' => $group->id,
                'created_by' => $user->id,
            ]);
        }
    }
}
