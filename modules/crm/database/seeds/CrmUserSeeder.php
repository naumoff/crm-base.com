<?php

namespace Crm\Database\Seeds;

use Crm\App\Models\CrmUser;

final class CrmUserSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        factory(CrmUser::class, 2)->states(CrmUser::STATUS_ACTIVE)->create();
        factory(CrmUser::class, 2)->states(CrmUser::STATUS_INVITED)->create();
        factory(CrmUser::class, 2)->states(CrmUser::STATUS_SUSPENDED)->create();
        factory(CrmUser::class, 2)->states(CrmUser::STATUS_REMOVED)->create();
    }
}
