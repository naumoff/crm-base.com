<?php

namespace Crm\Database\Seeds;

use App\User;
use Crm\App\Models\CrmUser;
use Illuminate\Support\Facades\DB;

class UserSeeder extends CrmSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        DB::connection('mysql')->table('users')->truncate();
        foreach (CrmUser::all() as $crmUser) {
            factory(User::class)->create([
                'id' => $crmUser->id,
                'name' => $crmUser->name,
                'email' => $crmUser->email,
                'email_verified_at' => $crmUser->email_verified_at,
                'password' => $crmUser->password,
                'database_id' => $crmUser->getDatabase()->id,
            ]);
        }
    }
}
