<?php

use Crm\App\Models\Lead;
use Illuminate\Support\Facades\Auth;

$nameSpace = 'Crm\Http\Controllers';

Route::group([
    'namespace' => $nameSpace,
    'prefix' => 'api/crm',
    'middleware' => ['api', 'bindings']
], function () {
    Route::get('/', function () {
        /** @var \App\User $user */
        $user = \App\User::find(2);
        Auth::login($user);
        dd($user->can(\Crm\App\Policies\CrmResourcePolicy::CREATE, Lead::class));
//        dd($user->can(
//                \Crm\App\Policies\CrmResourcePolicy::VIEW_COLLECTION,
//                [
//                    Lead::class,
//                    \Crm\App\Models\Lead::all()
//                ]
//            )
//        );
//        dd($user->can(\Crm\App\Policies\LeadResourcePolicy::VIEW, \Crm\App\Models\Lead::all()->random()));
    });
});
