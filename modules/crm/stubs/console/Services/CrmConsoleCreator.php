<?php

namespace Crm\Stubs\Console\Services;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use InvalidArgumentException;

class CrmConsoleCreator
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Create a new migration creator instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    /**
     * @param $name
     * @param $path
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function create($name, $path)
    {
        $this->ensureConsoleDoesntAlreadyExist($name, $path);

        $stub = $this->getStub();

        $this->files->put(
            $path = $this->getPath($name, $path),
            $this->populateStub($name, $stub)
        );

        return $path;
    }

    /**
     * @param $name
     * @param null $consolePath
     */
    protected function ensureConsoleDoesntAlreadyExist($name, $consolePath = null)
    {
        if (! empty($consolePath)) {
            $consoleFiles = $this->files->glob($consolePath.'/*.php');
            foreach ($consoleFiles as $consoleFile) {
                if ($name === basename($consoleFile, '.php')) {
                    throw new InvalidArgumentException("A {$name} class already exists.");
                }
            }
        }
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub(): string
    {
        return $this->files->get($this->stubPath().'/console.stub');
    }

    /**
     * Populate the place-holders in the migration stub.
     *
     * @param  string  $name
     * @param  string  $stub
     * @return string
     */
    protected function populateStub($name, $stub)
    {
        $stub = str_replace('DummyClass', $this->getClassName($name), $stub);
        $stub = str_replace('DummyNamespace', 'Crm\App\Console\Commands', $stub);

        return $stub;
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getClassName($name)
    {
        return Str::studly($name);
    }

    /**
     * Get the full path to the migration.
     *
     * @param  string  $name
     * @param  string  $path
     * @return string
     */
    protected function getPath($name, $path)
    {
        return $path.'/'.$name.'.php';
    }

    /**
     * Get the path to the stubs.
     *
     * @return string
     */
    public function stubPath()
    {
        return __DIR__.'/../';
    }
}
