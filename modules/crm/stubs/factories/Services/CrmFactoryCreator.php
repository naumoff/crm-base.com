<?php

namespace Crm\Stubs\Factories\Services;

use Illuminate\Filesystem\Filesystem;
use InvalidArgumentException;

class CrmFactoryCreator
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * Create a new migration creator instance.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    /**
     * @param string $name
     * @param string $path
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function create(string $name, string $path)
    {
        $name .= 'Factory';
        $this->ensureFactoryDoesntAlreadyExist($name, $path);

        $stub = $this->getStub();

        $this->files->put(
            $path = $this->getPath($name, $path),
            $this->populateStub($name, $stub)
        );

        return $path;
    }

    /**
     * @param string $newFactoryName
     * @param string $factoryPath
     */
    private function ensureFactoryDoesntAlreadyExist(string $newFactoryName, string $factoryPath): void
    {
        if (! empty($factoryPath)) {
            $factoryFiles = $this->files->glob($factoryPath.'/*.php');
            foreach ($factoryFiles as $factoryFile) {
                if ($newFactoryName === basename($factoryFile, '.php')) {
                    throw new InvalidArgumentException("Factory file {$newFactoryName} already exists!");
                }
            }
        }
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function getStub(): string
    {
        return $this->files->get($this->stubPath()."/create.stub");
    }

    /**
     * @param $name
     * @param $stub
     * @return string
     */
    private function populateStub($name, $stub): string
    {
        $stub = str_replace('DummyModelName', basename($name, 'Factory'), $stub);

        return $stub;
    }

    /**
     * Get the full path to the migration.
     *
     * @param  string  $name
     * @param  string  $path
     * @return string
     */
    protected function getPath($name, $path)
    {
        return $path.'/'.$name.'.php';
    }

    /**
     * Get the path to the stubs.
     *
     * @return string
     */
    public function stubPath()
    {
        return __DIR__.'/../';
    }
}
