<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/4/2020
 * TIME: 7:22 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\Stubs\Models\Services;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use InvalidArgumentException;

class CrmModelCreator
{
    #region PROPERTIES
    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    private Filesystem $files;
    #endregion

    #region MAIN METHODS
    /**
     * Create a new migration creator instance.
     *
     * @param  Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    /**
     * @param string $name
     * @param string $path
     * @return string
     * @throws FileNotFoundException
     */
    public function create(string $name, string $path): string
    {
        $this->ensureModelDoesntAlreadyExist($name, $path);

        [$modelStub, $repoStub, $collectionStub] = $this->getStub();

        $this->files->put(
            $this->getPath($name, $path),
            $this->populateModelStub($name, $modelStub)
        );

        $this->files->put(
            $this->getPath($name.'Repository', $path.'/Repositories'),
            $this->populateRepoStub($name, $repoStub)
        );

        $this->files->put(
            $this->getPath($name.'Collection', $path.'/Collections'),
            $this->populateCollectionStub($name, $collectionStub)
        );

        return $path;
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param string $name
     * @param string $servicePath
     * @param string|null $folder
     */
    private function ensureModelDoesntAlreadyExist(string $name, string $modelsPath): void
    {
        $modelsFiles = $this->files->glob($modelsPath.'/*.php');
        foreach ($modelsFiles as $modelFile) {
            if ($name === basename($modelFile, '.php')) {
                throw new InvalidArgumentException("Eloquent model {$name} already exists.");
            }
        }
    }

    /**
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function getStub(): array
    {
        $modelStub = $this->files->get($this->stubPath().'/model.stub');
        $repoStub = $this->files->get($this->stubPath().'/repo.stub');
        $collectionStub = $this->files->get($this->stubPath().'/collection.stub');
        return [$modelStub, $repoStub, $collectionStub];
    }

    /**
     * @param string $name
     * @param string $stub
     * @return string
     */
    private function populateModelStub(string $name, string $stub): string
    {
        $stub = str_replace('DummyClass', $this->getClassName($name), $stub);

        $stub = str_replace('DummyTable', $this->getTableName($name), $stub);

        $stub = str_replace('DummyNamespace', 'Crm\App\Models', $stub);

        $stub = str_replace(
            'use DummyCollectionNamespace;',
            'use Crm\App\Models\Collections\\' . $this->getClassName($name) . 'Collection;',
            $stub
        );

        $stub = str_replace('DummyCollection', $this->getClassName($name).'Collection', $stub);

        return $stub;
    }

    /**
     * @param string $name
     * @param string $stub
     * @return string
     */
    private function populateRepoStub(string $name, string $stub): string
    {
        $stub = str_replace(
            'use DummyClassNamespace;',
            'use Crm\App\Models\\' . $this->getClassName($name) .';',
            $stub
        );

        $stub = str_replace('DummyClass', $this->getClassName($name), $stub);

        $stub = str_replace('DummyNamespace', 'Crm\App\Models\Repositories', $stub);

        return $stub;
    }

    /**
     * @param string $name
     * @param string $stub
     * @return string
     */
    private function populateCollectionStub(string $name, string $stub): string
    {

        $stub = str_replace('DummyClass', $this->getClassName($name), $stub);

        $stub = str_replace('DummyNamespace', 'Crm\App\Models\Collections', $stub);

        return $stub;
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    private function getClassName($name)
    {
        return Str::studly($name);
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    private function getTableName($name)
    {
        return Str::plural(Str::snake($name));
    }

    /**
     * Get the full path to the migration.
     *
     * @param  string  $name
     * @param  string  $path
     * @return string
     */
    private function getPath($name, $path)
    {
        return $path.'/'.$name.'.php';
    }

    /**
     * Get the path to the stubs.
     *
     * @return string
     */
    private function stubPath()
    {
        return __DIR__.'/../';
    }
    #endregion
}
