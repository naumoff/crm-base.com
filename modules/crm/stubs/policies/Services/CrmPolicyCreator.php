<?php
/**
 * USER: Andrey Naumoff
 * DATE: 2/4/2020
 * TIME: 7:22 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Crm\Stubs\Policies\Services;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use InvalidArgumentException;

class CrmPolicyCreator
{
    #region PROPERTIES
    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    private Filesystem $files;
    #endregion

    #region MAIN METHODS
    /**
     * Create a new migration creator instance.
     *
     * @param  Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    /**
     * @param string $policyName
     * @param Model|null $model
     * @return string
     * @throws FileNotFoundException
     * @throws \ReflectionException
     */
    public function create(string $policyName, ?Model $model): string
    {
        $this->ensurePolicyDoesntAlreadyExist($policyName);
        $policyStub = $this->getStub($model);
        if (! empty($model)) {
            $this->files->put(
                $this->getPath($policyName),
                $this->populatePolicyModelStub($policyStub, $policyName, $model)
            );
        } else {
            $this->files->put(
                $this->getPath($policyName),
                $this->populatePolicyStub($policyStub, $policyName)
            );
        }
        return basename($this->getPath($policyName), '.php');
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param string $policyName
     */
    private function ensurePolicyDoesntAlreadyExist(string $policyName): void
    {
        $policyFiles = $this->files->glob(base_path('modules/crm/app/Policies').'/*.php');
        foreach ($policyFiles as $policyFile) {
            if ($policyName === basename($policyFile, '.php')) {
                throw new InvalidArgumentException("Policy {$policyName} already exists.");
            }
        }
    }

    /**
     * @param Model|null $model
     * @return string
     * @throws FileNotFoundException
     */
    private function getStub(?Model $model): string
    {
        if (! empty($model)) {
            $policyStub = $this->files->get($this->stubPath(). '/policy.resource.stub');
        } else {
            $policyStub = $this->files->get($this->stubPath(). '/policy.stub');
        }
        return $policyStub;
    }

    /**
     * @param string $stub
     * @param string $policyName
     * @param Model $model
     * @return string
     * @throws \ReflectionException
     */
    private function populatePolicyModelStub(string $stub, string $policyName, Model $model): string
    {
        $stub = str_replace('DummyPolicy', $policyName, $stub);

        $stub = str_replace('DummyResource', $this->getClassName($model), $stub);

        $stub = str_replace('$resource', '$'. lcfirst($this->getClassName($model)), $stub);

        return $stub;
    }

    /**
     * @param $stub
     * @param $policyName
     * @return string
     */
    private function populatePolicyStub($stub, $policyName): string
    {
        $stub = str_replace('DummyPolicy', $policyName, $stub);

        return $stub;
    }

    /**
     * @param Model $model
     * @return string
     * @throws \ReflectionException
     */
    private function getClassName(Model $model): string
    {
        $function = new \ReflectionClass(get_class($model));
        return $function->getShortName();
    }

    /**
     * @param $policyName
     * @return string
     */
    private function getPath($policyName): string
    {
        return base_path('modules/crm/app/Policies/' . $policyName . '.php');
    }

    /**
     * Get the path to the stubs.
     *
     * @return string
     */
    private function stubPath(): string
    {
        return __DIR__.'/../';
    }
    #endregion
}
