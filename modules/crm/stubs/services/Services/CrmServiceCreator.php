<?php

namespace Crm\Stubs\Services\Services;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class CrmServiceCreator
{
    #region PROPERTIES
    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    private Filesystem $files;
    #endregion

    #region MAIN METHODS
    /**
     * Create a new migration creator instance.
     *
     * @param  Filesystem  $files
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    /**
     * @param string|null $folder
     * @param string $name
     * @param string $path
     * @return string
     * @throws FileNotFoundException
     */
    public function create(?string $folder, string $name, string $path): string
    {
        $path = $fullPath = ($folder)? $path .'/'.$folder : $path;

        $this->ensureServiceDoesntAlreadyExist($name, $path);

        [$serviceStub, $contractStub] = $this->getStub();

        $this->createFoldersIfNotExist($path, $folder);

        $this->files->put(
            $pathOne = $this->getPath($name.'Service', $path),
            $this->populateServiceStub($folder, $name, $serviceStub)
        );

        if ($folder) { // we create contracts only for services in separate folder
            $this->files->put(
                $pathTwo = $this->getPath($name.'Contract', $path.'/Contracts'),
                $this->populateContractStub($folder, $name, $contractStub)
            );
        }
        return $path;
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param string $name
     * @param string $servicePath
     * @param string|null $folder
     */
    private function ensureServiceDoesntAlreadyExist(string $name, string $servicePath): void
    {
        $serviceFiles = $this->files->glob($servicePath.'/*.php');
        foreach ($serviceFiles as $serviceFile) {
            if ($name === basename($serviceFile, 'Service.php')) {
                throw new InvalidArgumentException("A {$name}Service class already exists.");
            }
        }
    }

    /**
     * @param string $path
     * @param string|null $folder
     */
    private function createFoldersIfNotExist(string $path, ?string $folder): void
    {
        if (!$this->files->exists($path)) {
            $this->files->makeDirectory($path);
        }

        if ($folder) {
            if (!$this->files->exists($path.'/Contracts')) {
                $this->files->makeDirectory($path.'/Contracts');
            }
        }
    }

    /**
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function getStub(): array
    {
        $serviceStub = $this->files->get($this->stubPath().'/service.stub');
        $contractStub = $this->files->get($this->stubPath().'/contract.stub');
        return [$serviceStub, $contractStub];
    }

    /**
     * @param string|null $folder
     * @param string $name
     * @param string $stub
     * @return string
     */
    private function populateServiceStub(?string $folder, string $name, string $stub): string
    {
        $stub = str_replace('DummyClass', $this->getClassName($name).'Service', $stub);

        if ($folder) {
            $use = 'use Crm\App\Services\\' . ucfirst($folder) . '\\Contracts\\' . $this->getClassName($name) . 'Contract;';
        } else {
            $use = null;
        }

        $stub = str_replace('use DummyInterfaceNamespace;', $use, $stub);

        $interface = ($folder)? "implements {$this->getClassName($name)}Contract" : '';
        $stub = str_replace('implements DummyInterface', $interface, $stub);

        $namespace = ($folder)? 'Crm\App\Services\\' . $folder : 'Crm\App\Services';
        $stub = str_replace('DummyNamespace', $namespace, $stub);
        return $stub;
    }

    /**
     * @param string|null $folder
     * @param string $name
     * @param string $stub
     * @return string
     */
    private function populateContractStub(string $folder, string $name, string $stub): string
    {
        $stub = str_replace('DummyInterface', $this->getClassName($name).'Contract', $stub);
        $namespace = 'Crm\App\Services\\' . $folder .'\\Contracts';
        $stub = str_replace('DummyNamespace', $namespace, $stub);
        return $stub;
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    private function getClassName($name)
    {
        return Str::studly($name);
    }

    /**
     * Get the full path to the migration.
     *
     * @param  string  $name
     * @param  string  $path
     * @return string
     */
    private function getPath($name, $path)
    {
        return $path.'/'.$name.'.php';
    }

    /**
     * Get the path to the stubs.
     *
     * @return string
     */
    private function stubPath()
    {
        return __DIR__.'/../';
    }
    #endregion
}
